// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Common
 * \brief Get only parts of a container or tuple
 */
#ifndef DUMUX_COMMON_PARTIAL_HH_XT
#define DUMUX_COMMON_PARTIAL_HH_XT

#include <tuple>
#include <type_traits>

#include <dune/istl/multitypeblockvector.hh>
#include <dumux/common/partial.hh>

namespace Dumux {


template<class B, class A=std::allocator<B>, std::size_t i_0>
auto partialcopy(const Dune::BlockVector<B, A>& v, Dune::BlockVector<B, A>& bv, std::size_t ix, Dune::index_constant<i_0> index_0)
{
    bv[ix] = v[index_0];
}

template<class B, class A=std::allocator<B>, std::size_t i_0, std::size_t ...i>
auto partialcopy(const Dune::BlockVector<B, A>& v, Dune::BlockVector<B, A>& bv, std::size_t ix, Dune::index_constant<i_0> index_0, Dune::index_constant<i>... indices)
{
    bv[ix] = v[index_0];
    partialcopy<B,A,i...>(v, bv, ix+1, indices...);
}


template<class B, class A=std::allocator<B>, std::size_t i_0>
auto partialcopy(Dune::BlockVector<B, A>& v, Dune::BlockVector<B, A>& bv, std::size_t ix, Dune::index_constant<i_0> index_0)
{
    bv[ix] = v[index_0];
}

template<class B, class A=std::allocator<B>, std::size_t i_0, std::size_t ...i>
auto partialcopy(Dune::BlockVector<B, A>& v, Dune::BlockVector<B, A>& bv, std::size_t ix, Dune::index_constant<i_0> index_0, Dune::index_constant<i>... indices)
{
    bv[ix] = v[index_0];
    partialcopy<B,A,i...>(v, bv, ix+1, indices...);
}
/*!
 * \brief a function to get a BlockVector with some entries of another BlockVector
 * \param v a BlockVector
 * \param indices the indices of the entries that should be copied
 */
template<class B, class A=std::allocator<B>, std::size_t ...i>
auto partial(Dune::BlockVector<B, A>& v, Dune::index_constant<i>... indices)
{
    Dune::BlockVector<B, A> bv(sizeof...(i));
    partialcopy<B,A,i...>(v, bv, 0, indices...);
    return bv;
}


/*!
 * \brief a function to get a non-const BlockVector with some entries of another const BlockVector
 * \param v a const BlockVector
 * \param indices the indices of the entries that should be copied
 */
template<class B, class A=std::allocator<B>, std::size_t ...i>
auto partial(const Dune::BlockVector<B, A>& v, Dune::index_constant<i>... indices)
{
    Dune::BlockVector<B, A> bv(sizeof...(i));
    partialcopy<B,A,i...>(v, bv, 0, indices...);
    return bv;
}


} // end namespace Dumux

#endif
