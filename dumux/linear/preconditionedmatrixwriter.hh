// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Linear
 * \brief A helper classe that calculates the matrix representation of a preconditioned matrix, even if the preconditioner is matrix-free.
 */
#ifndef DUMUX_PRECONDITIONED_MATRIX_WRITER
#define DUMUX_PRECONDITIONED_MATRIX_WRITER

#include <dune/istl/bvector.hh>
#include <dune/istl/preconditioner.hh>

#include <dune/istl/io.hh>
#include <dune/istl/matrixmarket.hh>

#include <iostream>
#include <fstream>


namespace Dumux {

/*!
 * \ingroup Linear
 * \brief 
 *
 */
  template <class M, class X, class Scalar=double>
  class PreconditionedMatrix
  {
    using VectorBlock = typename Dune::FieldVector<Scalar, 1>;
    using BlockVector = typename Dune::BlockVector<VectorBlock>;
    using BlockBlockVector = typename Dune::BlockVector<BlockVector>;

  public:
    PreconditionedMatrix (const M& A_, Dune::Preconditioner<X, X>& prec_)
      : A(A_), prec(prec_), unityvector(3), matcol(3)
    {
      set_vecdims();
      
      // Initialize matcol to fit dimensions of A
      for(int b=0; b<num_blocks; b++){
        int blocksize = A[0][b].M();
        BlockVector block(blocksize);
        for(int e=0; e<blocksize; e++){
          block[e]=0;
        }
        matcol[b] = block;
      }

      // Initialize first unity vector to fit dimensions of A
      int unitynum=1;
      int blockstartidx = 0;
      for (int b = 0; b<num_blocks; b++){
        int blocksize = A[0][b].M();
        BlockVector block(blocksize);
        if (unitynum >= blockstartidx and unitynum < blockstartidx+blocksize){
          int idty = unitynum - blockstartidx;
          for(int e = 0; e<blocksize; e++){
            if (idty == e)
              block[e] = 1.0;
            else
              block[e] = 0.0;
          }
        }
        else{
          for(int e = 0; e<blocksize; e++){
            block[e] = 0.0;
          }
        }
        unityvector[b] = block;
        blockstartidx += blocksize;
      }
      std::cout << "Initialized all" << std::endl;
    }


  public:
    void save_matrix(){
      std::cout << num_blocks << std::endl;
      std::cout << num_vecentries << std::endl;

      std::ofstream outstream(outfilename, std::ios::binary);
    
      int64_t dump = (int64_t) num_vecentries;
      outstream.write((char*)&dump, sizeof(int64_t));
      dump = (int64_t) num_blocks;
      outstream.write((char*)&dump, sizeof(int64_t));

      for (int b=0; b<num_blocks; b++){
        dump = (int64_t) unityvector[b].N();
        outstream.write((char*)&dump, sizeof(int64_t));
        std::cout << unityvector[b].N() << " ";
      }
      std::cout << std::endl;

      for (int i = 0; i<num_vecentries; i++){
         // set matcol to zero to prevent side effects from preconditioners
        for(int b=0; b<num_blocks; b++){
          int blocksize = A[0][b].M();
          BlockVector block(blocksize);
          for(int e=0; e<blocksize; e++){
            block[e]=0;
          }
          matcol[b] = block;
        }
        // Initializing intermediate with 0 also prevents side effects from preconditioners
        X intermediate(num_blocks);
        for(int b=0; b<num_blocks; b++){
          int blocksize = A[0][b].M();
          BlockVector block(blocksize);
          for(int e=0; e<blocksize; e++){
            block[e]=0;
          }
          intermediate[b] = block;
        }
        apply_preconditioner(A, prec, intermediate);
        
        for (int b=0; b<num_blocks; b++){
          for (int j=0; j<matcol[b].N(); j++){
            outstream.write((char*)&matcol[b][j], sizeof(double));
          }
        }

        if (i<num_vecentries-1){
          update_unityvec(i+1, i);
          // std::cout << "Updated Unityvector from " << i << " to " << i+1 << std::endl;
        }
      }
      outstream.close();
    }

    void update_outfilename(std::string newoutfilename){
      outfilename = newoutfilename;
    }

  private:
    void set_vecdims()
    {
      num_blocks = A.M();
      num_vecentries = 0;
      for (int i = 0; i<num_blocks; i++){
        num_vecentries += A[0][i].M();
      }
    }

    void update_unityvec(int unitynum_new, int unitynum_prev){
      int blockstartidx = 0;
      for (int b = 0; b<num_blocks; b++){
        int blocksize = A[0][b].M();
        if (unitynum_new >= blockstartidx and unitynum_new < blockstartidx+blocksize){
          int unityidx_new = unitynum_new - blockstartidx;
          unityvector[b][unityidx_new] = 1.0;
        }
        if (unitynum_prev >= blockstartidx and unitynum_prev < blockstartidx+blocksize){
          int unityidx_prev = unitynum_prev - blockstartidx;
          unityvector[b][unityidx_prev] = 0.0;
        }
        blockstartidx += blocksize;
      }

    }

    void write_matrixcolumn(int columnumber){

    }

    virtual void apply_preconditioner(const M& A, Dune::Preconditioner<X, X>& prec, X& intermediate){
      // std::cout << "apply precond" << std::endl;
    }

  public:
    const M& A;
    Dune::Preconditioner<X, X>& prec;
    X unityvector;
    X matcol;
    int num_blocks = 0;
    int num_vecentries = 0;
    std::string outfilename = "matrixstructure.txt";

  };





  template <class M, class X>
  class RightPreconditionedMatrix : public PreconditionedMatrix<M, X>
  {
  public:
    RightPreconditionedMatrix (const M& A_, Dune::Preconditioner<X,X>& prec_) 
      : PreconditionedMatrix<M, X>(A_, prec_){}

    RightPreconditionedMatrix (const M& A_, Dune::Preconditioner<X,X>& prec_, bool matrix_only, bool preconditioner_only)
      : PreconditionedMatrix<M, X>(A_, prec_), onlymatrix(matrix_only), onlyprecond(preconditioner_only) {}

  private:
    virtual void apply_preconditioner(const M& A, Dune::Preconditioner<X,X>& prec, X& intermediate){
      // std::cout << "apply right precond" << std::endl;
      if (onlymatrix)
        A.mv(this->unityvector, this->matcol); // matrix only
      else if (onlyprecond){
        prec.pre(this->matcol, this->unityvector);
        prec.apply(this->matcol,this->unityvector); // preconditioner only
       }
      else{
        prec.pre(intermediate,this->unityvector); // preconditioned matrix
        prec.apply(intermediate,this->unityvector); // preconditioned matrix
        A.mv(intermediate, this->matcol); // preconditioned matrix
      }
    }

    bool onlymatrix = false;
    bool onlyprecond = false;
  };





  template <class M, class X>
  class LeftPreconditionedMatrix : public PreconditionedMatrix<M, X>
  {
  public:
    LeftPreconditionedMatrix (const M& A_, Dune::Preconditioner<X,X>& prec_)
      : PreconditionedMatrix<M, X>(A_, prec_){}

    LeftPreconditionedMatrix (const M& A_, Dune::Preconditioner<X,X>& prec_, bool matrix_only, bool preconditioner_only)
      : PreconditionedMatrix<M, X>(A_, prec_), onlymatrix(matrix_only), onlyprecond(preconditioner_only) {}

  private:
    virtual void apply_preconditioner(const M& A, Dune::Preconditioner<X,X>& prec, X& intermediate){
      // std::cout << "apply left precond" << std::endl;
      if (onlymatrix)
        A.mv(this->unityvector, this->matcol); // matrix only
      else if (onlyprecond){
        prec.pre(this->matcol, this->unityvector);
        prec.apply(this->matcol,this->unityvector); // preconditioner only
       }
      else{
        A.mv(this->unityvector, intermediate); // preconditioned matrix
        prec.pre(this->matcol,intermediate); // preconditioned matrix
        prec.apply(this->matcol,intermediate); // preconditioned matrix
      }
    }

    bool onlymatrix = false;
    bool onlyprecond = false;
  };








  template <class M, class X, class Scalar=double>
  class PreconditionedSingleMatrix
  {
    using VectorBlock = typename Dune::FieldVector<Scalar, 1>;
    using BlockVector = typename Dune::BlockVector<VectorBlock>;
    using BlockBlockVector = typename Dune::BlockVector<BlockVector>;

  public:
    PreconditionedSingleMatrix (const M& A_, Dune::Preconditioner<X, X>& prec_)
      : A(A_), prec(prec_), unityvector(A.M()), matcol(A.N())
    {
      set_vecdims();

      // initialize unityvector
      unityvector[0]=1.0;
      for(int e = 1; e<num_vecentries; e++){
        unityvector[e] = 0.0;
      }

      //initialize matcol
      for(int e = 0; e<num_vecentries; e++)
        matcol[e] = 0.0;
      std::cout << "Initialized all" << std::endl;
    }


  public:
    void save_matrix(){
      std::cout << num_blocks << std::endl;
      std::cout << num_vecentries << std::endl;

      std::ofstream outstream(outfilename, std::ios::binary);
      int64_t dump = (int64_t) num_vecentries;
      outstream.write((char*)&dump, sizeof(int64_t));

      dump = (int64_t) 1;
      outstream.write((char*)&dump, sizeof(int64_t));
      dump = (int64_t) num_vecentries;
      outstream.write((char*)&dump, sizeof(int64_t));
      std::cout << std::endl;

      X intermediate(num_vecentries);
      for(int e=0; e<num_vecentries; e++){
        intermediate[e] = 0.0;
      }

      for (int i = 0; i<num_vecentries; i++){
        // set matcol and intermediate to zero so that preconditioner operator doesn't get something else but D^-1 for JAcobi (depending on left- or right preconditioning, matcol or intermediate takes the role of x in p^-1 x = b)
        for(int e = 0; e<num_vecentries; e++){
          matcol[e] = 0.0;
          intermediate[e] = 0.0;
        }
        apply_preconditioner(A, prec, intermediate);

        for (int j=0; j<matcol.N(); j++){
          outstream.write((char*)&matcol[j], sizeof(double));
        }

        if (i<num_vecentries-1){
          update_unityvec(i+1, i);
          // std::cout << "Updated Unityvector from " << i << " to " << i+1 << std::endl;
        }
      }
      outstream.close();
    }

    void update_outfilename(std::string newoutfilename){
      outfilename = newoutfilename;
    }

  private:
    void set_vecdims()
    {
      num_blocks = 1;
      num_vecentries = A.M();
    }

    void update_unityvec(int unitynum_new, int unitynum_prev){
      unityvector[unitynum_new] = 1.0;
      unityvector[unitynum_prev] = 0.0;
    }

    virtual void apply_preconditioner(const M& A, Dune::Preconditioner<X, X>& prec, X intermediate){
      std::cout << "apply precond" << std::endl;
    }

  public:
    const M& A;
    Dune::Preconditioner<X, X>& prec;
    X unityvector;
    X matcol;
    int num_blocks = 1;
    int num_vecentries = 0;
    std::string outfilename = "matrixstructure.txt";

  };





  template <class M, class X>
  class RightPreconditionedSingleMatrix : public PreconditionedSingleMatrix<M, X>
  {
  public:
    RightPreconditionedSingleMatrix (const M& A_, Dune::Preconditioner<X,X>& prec_) 
      : PreconditionedSingleMatrix<M, X>(A_, prec_){}

    RightPreconditionedSingleMatrix (const M& A_, Dune::Preconditioner<X,X>& prec_, bool matrix_only, bool preconditioner_only)
      : PreconditionedSingleMatrix<M, X>(A_, prec_), onlymatrix(matrix_only), onlyprecond(preconditioner_only) {}

  private:
    virtual void apply_preconditioner(const M& A, Dune::Preconditioner<X,X>& prec, X intermediate){
      // std::cout << "apply right precond" << std::endl;
      if (onlymatrix)
        A.mv(this->unityvector, this->matcol); // matrix only
      else if (onlyprecond){
        prec.pre(this->matcol, this->unityvector);
        prec.apply(this->matcol,this->unityvector); // preconditioner only
       }
      else{
        prec.pre(intermediate,this->unityvector); // preconditioned matrix
        prec.apply(intermediate,this->unityvector); // preconditioned matrix
        A.mv(intermediate, this->matcol); // preconditioned matrix
      }
    }

    bool onlymatrix = false;
    bool onlyprecond = false;
  };





  template <class M, class X>
  class LeftPreconditionedSingleMatrix : public PreconditionedSingleMatrix<M, X>
  {
  public:
    LeftPreconditionedSingleMatrix (const M& A_, Dune::Preconditioner<X,X>& prec_)
      : PreconditionedSingleMatrix<M, X>(A_, prec_){}

    LeftPreconditionedSingleMatrix (const M& A_, Dune::Preconditioner<X,X>& prec_, bool matrix_only, bool preconditioner_only)
      : PreconditionedSingleMatrix<M, X>(A_, prec_), onlymatrix(matrix_only), onlyprecond(preconditioner_only) {}

  private:
    virtual void apply_preconditioner(const M& A, Dune::Preconditioner<X,X>& prec, X intermediate){
      // std::cout << "apply left precond" << std::endl;
      if (onlymatrix)
        A.mv(this->unityvector, this->matcol); // matrix only
      else if (onlyprecond){
        prec.pre(this->matcol, this->unityvector);
        prec.apply(this->matcol,this->unityvector); // preconditioner only
       }
      else{
        A.mv(this->unityvector, intermediate); // preconditioned matrix
        prec.pre(this->matcol,intermediate); // preconditioned matrix
        prec.apply(this->matcol,intermediate); // preconditioned matrix
      }
    }

    bool onlymatrix = false;
    bool onlyprecond = false;
    
  };


} // end namespace Dumux

#endif