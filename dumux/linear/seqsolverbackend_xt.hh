// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Linear
 * \brief Dumux sequential linear solver backends
 */
#ifndef DUMUX_SEQ_SOLVER_BACKEND_HH_XT
#define DUMUX_SEQ_SOLVER_BACKEND_HH_XT

#include <dumux/linear/seqsolverbackend.hh>

#include <dune/istl/preconditioners_xt.hh>
#include <dune/istl/solvers.hh>
#include <dune/istl/solvers_xt.hh>

#include <dune/istl/io.hh>
#include <dune/common/indices.hh>

namespace Dumux {

// /*!
//  * \ingroup Linear
//  * \brief Sequential BlockyPreconditioner-preconditioned GMRes solver.
//  *
//  * Solver: The GMRes (generalized minimal residual) method is an iterative
//  * method for the numerical solution of a nonsymmetric system of linear
//  * equations.\n
//  * See: Saad, Y., Schultz, M. H. (1986). "GMRES: A generalized minimal residual
//  * algorithm for solving nonsymmetric linear systems." SIAM J. Sci. and Stat.
//  * Comput. 7: 856–869.
//  *
//  * Preconditioner: ILU(0) incomplete LU factorization. The order 0 indicates
//  * that no fill-in is allowed. It can be damped by the relaxation parameter
//  * LinearSolver.PreconditionerRelaxation.\n
//  * See: Golub, G. H., and Van Loan, C. F. (2012). Matrix computations. JHU Press.
//  */
// template<class P, class Subvector>
// class BlockyPrecRestartedGMResBackend : public LinearSolver
// {
// public:
//     using LinearSolver::LinearSolver;

//     BlockyPrecRestartedGMResBackend(P& precvec) : precvec_(precvec){}

//     // precondBlockLevel: set this to more than one if the matrix to solve is nested multiple times
//     template<int precondBlockLevel = 1, class Matrix, class Vector>
//     bool solve(const Matrix& A, Vector& x, const Vector& b)
//     {
//         // using Preconditioner = Dune::BlockyPreconditioner<Matrix, Vector, Vector, precondBlockLevel>;
//         using Preconditioner = Dune::BlockyPreconditioner<Matrix, Vector, Vector, Subvector, Subvector, P>
//         using Solver = Dune::RestartedGMResSolver<Vector>;

//         return IterativePreconditionedSolverImpl::template solveWithBlockyPrecGMRes<Preconditioner, Solver, P>(*this, A, x, b, this->paramGroup(), precvec_);
//     }

//     std::string name() const
//     {
//         return "BlockyPreconditioner preconditioned BiCGSTAB solver";
//     }
// private:
//     P& precvec_;
// };


// #if HAVE_UMFPACK

template<class M, class X, class Y, class GridGeometry>
class SeqUzawa : public Dune::Preconditioner<X,Y>
{
    using VelocityMatrix = typename std::remove_reference<decltype(std::declval<M>()[Dune::Indices::_1][Dune::Indices::_1])>::type;
    using VelocityVector = typename std::remove_reference<decltype(std::declval<X>()[Dune::Indices::_1])>::type;

    static constexpr bool isParallel = false;
    using SolverTraits = NonoverlappingSolverTraits<VelocityMatrix, VelocityVector, isParallel>;
    using Comm = typename SolverTraits::Comm;
    using LinearOperator = typename SolverTraits::LinearOperator;
    using ScalarProduct = typename SolverTraits::ScalarProduct;
    using Smoother = typename SolverTraits::Smoother;
    using VelocityAMG = Dune::Amg::AMG<LinearOperator, VelocityVector, Smoother,Comm>;
    // using VelocityUMFPack = Dune::UMFPack<VelocityMatrix>;
    typedef Dune::Amg::DirectSolverSelector<VelocityMatrix, VelocityVector> SolverSelector;
    typedef Dune::InverseOperator<VelocityVector, VelocityVector> DirectSolver;

public:
    //! \brief The matrix type the preconditioner is for.
    typedef M matrix_type;
    //! \brief The domain type of the preconditioner.
    typedef X domain_type;
    //! \brief The range type of the preconditioner.
    typedef Y range_type;
    //! \brief The field type of the preconditioner.
    typedef typename X::field_type field_type;
    //! \brief scalar type underlying the field_type
    typedef Dune::Simd::Scalar<field_type> scalar_field_type;

    /*! \brief Constructor.
     *
     *   constructor gets all parameters to operate the prec.
     *   \param A The matrix to operate on.
     *   \param n The number of iterations to perform.
     *   \param w The relaxation factor.
     */
    SeqUzawa (const M& A, int n, scalar_field_type w)
    : _A_(A), _n(n), _w(w)
    {
        using namespace Dune::Indices;  // for _0, _1, etc.
        inexact_ = getParam<bool>("LinearSolver.InexactVelocitySolver", false);

        if (!inexact_ && !SolverSelector::isDirectSolver) {  // using inexact solver when no direct solver available
            std::cout << "No direct solver available. Fall-back to inexact Uzawa. Consider installing SuperLU or UMFPack." << std::endl;
            inexact_ = true;
        }

        if (inexact_)
        {
            Dune::Amg::Parameters params(15, 2000, 1.2, 1.6, Dune::Amg::atOnceAccu);
            params.setDefaultValuesIsotropic(GridGeometry::GridView::dimension);
            params.setDebugLevel(0);

            using Criterion = Dune::Amg::CoarsenCriterion<Dune::Amg::SymmetricCriterion<VelocityMatrix, Dune::Amg::FirstDiagonal>>;
            Criterion criterion(params);

            using SmootherArgs = typename Dune::Amg::SmootherTraits<Smoother>::Arguments;
            SmootherArgs smootherArgs;
            smootherArgs.iterations = 1;
            smootherArgs.relaxationFactor = 1;

            linearOperator_ = std::make_unique<LinearOperator>(_A_[_1][_1]);
            velocityAMG_ = std::make_unique<VelocityAMG>(*linearOperator_, criterion, smootherArgs, comm_);
        }
        else
        {
            // velocityUMFPack_ = std::make_unique<VelocityUMFPack>(_A_[_1][_1]);
            directSolver_.reset(SolverSelector::create(_A_[_1][_1], false, false));
        }
    }

    /*!
     *   \brief Prepare the preconditioner.
     *
     *   \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b) {}

    /*!
     *   \brief Apply the preconditioner
     *
     *   \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d)
    {
        using namespace Dune::Indices;

        auto& A = _A_[_1][_1];
        auto& Bt = _A_[_1][_0];
        auto& B = _A_[_0][_1];
        auto& C = _A_[_0][_0];

        const auto& f = d[_1];
        const auto& g = d[_0];
        auto& velocity = v[_1];
        auto& pressure = v[_0];
        for (int i = 0; i < C.N(); ++i)
        {
            if (std::abs(C[i][i].frobenius_norm() - 1.0) < 1e-14)
                pressure[i] = g[i];
        }

        for (int k = 0; k < _n; ++k)
        {
            // u_k+1 = u_k + Q_A^−1*(f − (A*u_k + Bt*p_k)),
            auto vrhs = f;
            A.mmv(velocity, vrhs);
            Bt.mmv(pressure, vrhs);
            auto vUpdate = velocity;
            if (inexact_)
            {
                velocityAMG_->pre(vUpdate, vrhs);
                velocityAMG_->apply(vUpdate, vrhs);
                velocityAMG_->post(vUpdate);
            }
            else
            {
                Dune::InverseOperatorResult res;
                // velocityUMFPack_->apply(vUpdate, vrhs, res);
                directSolver_->apply(vUpdate, vrhs, res);
            }
            velocity += vUpdate;

            // p_k+1 = p_k + omega*(g - B*u_k+1 - C*p_k)
            auto pUpdate = g;
            B.mmv(velocity, pUpdate);
            C.mmv(pressure, pUpdate);
            pUpdate *= _w;
            pressure += pUpdate;
        }
    }

    /*!
     *   \brief Clean up.
     *
     *   \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x) {}

    //! Category of the preconditioner (see SolverCategory::Category)
    virtual Dune::SolverCategory::Category category() const
    {
        return Dune::SolverCategory::sequential;
    }

private:
    //! \brief The matrix we operate on.
    const M& _A_;
    //! \brief The number of steps to do in apply
    int _n;
    //! \brief The relaxation factor to use
    scalar_field_type _w;

    Comm comm_;
    std::unique_ptr<LinearOperator> linearOperator_;
    std::unique_ptr<VelocityAMG> velocityAMG_;
    // std::unique_ptr<VelocityUMFPack> velocityUMFPack_;
    std::shared_ptr<DirectSolver> directSolver_;
    bool inexact_;
};


template <class GridGeometry>
class UzawaSolver : public LinearSolver
{
public:
    using LinearSolver::LinearSolver;

    template<int precondBlockLevel = 1, class Matrix, class Vector>
    bool solve(const Matrix& A, Vector& x, const Vector& b)
    {
        using namespace Dune::Indices; // for _0, _1, etc.
        static bool accelerated = getParamFromGroup<bool>(this->paramGroup(),
                                                          "LinearSolver.AndersonAcceleration",
                                                          false);

        auto res = b;
        A.mmv(x, res);
        auto initRes = res.two_norm();
        if (this->verbosity() > 1)
        {
            if (accelerated)
                std::cout << "\nAnderson-accelerated ";
            else
                std::cout << "\nStandard ";
            std::cout << "UzawaSolver starts." << std::endl;
            std::cout << "0: " << initRes << std::endl;
        }

        using Preconditioner = SeqUzawa<Matrix, Vector, Vector, GridGeometry>;
        Preconditioner precond(A, this->precondIter(), this->relaxation());
        const auto maxIter = this->maxIter();

        if (!accelerated)
        {
            for (int k = 1; k < maxIter; ++k)
            {
                precond.apply(x, b);

                res = b;
                A.mmv(x, res);
                auto residual = res.two_norm();
                if (this->verbosity() > 1)
                    std::cout << k << ": " << residual << std::endl;

                if (residual/initRes < this->residReduction())
                {
                    if (this->verbosity() > 0)
                        std::cout << "UzawaSolver finished in " << k << " iterations." << std::endl;
                    return true;
                }
            }
        }
        else
        {
            const auto size = x[_0].size() + x[_1].size();
            const auto m = 10;

            // Given xi0 and m >= 1, set xi1 = G(x0)
            // For k = 1, 2, ...
            //   Set mk = min{m, k}.
            //   Set Fk = (fk−mk, ..., fk), where fi = G(xi) − xi.
            //   Determine alpha(k) = (alpha0(k), ..., alphamk(k))T
            //     that solves min | Fk.alpha | s.t. sum alphai = 1
            //   Set xk+1 = sum alphai(k).G(xk−mk+i).

            using Scalar = typename Vector::field_type;
            Dune::DynamicMatrix<Scalar> xi(maxIter, size);
            Dune::DynamicMatrix<Scalar> Gk(maxIter, size);

            std::copy(x[_0].begin(), x[_0].end(), xi[0].begin());
            std::copy(x[_1].begin(), x[_1].end(), xi[0].begin() + x[_0].size());
            precond.apply(x, b);
            std::copy(x[_0].begin(), x[_0].end(), xi[1].begin());
            std::copy(x[_1].begin(), x[_1].end(), xi[1].begin() + x[_0].size());
            Gk[0] = xi[1];

            for (int k = 1; k < maxIter; ++k)
            {
                std::copy(xi[k].begin(), xi[k].begin() + x[_0].size(), x[_0].begin());
                std::copy(xi[k].begin() + x[_0].size(), xi[k].end(), x[_1].begin());

                res = b;
                A.mmv(x, res);
                auto residual = res.two_norm();
                if (this->verbosity() > 1)
                    std::cout << k << ": " << residual << std::endl;

                if (residual/initRes < this->residReduction())
                {
                    if (this->verbosity() > 0)
                        std::cout << "UzawaSolver finished in " << k << " iterations." << std::endl;
                    return true;
                }

                precond.apply(x, b);
                std::copy(x[_0].begin(), x[_0].end(), Gk[k].begin());
                std::copy(x[_1].begin(), x[_1].end(), Gk[k].begin() + x[_0].size());

                const auto mk = std::min(m, k);
                Dune::DynamicMatrix<Scalar> FkT(mk+1, size);
                for (size_t i = 0; i <= mk; ++i)
                {
                    FkT[i] = Gk[k - mk + i] - xi[k - mk + i];
                }

                Dune::DynamicMatrix<Scalar> FkTFk(mk+1, mk+1);
                for (size_t i = 0; i <= mk; ++i)
                    for (size_t j = 0; j <= mk; ++j)
                        FkTFk[i][j] = FkT[i].dot(FkT[j]);

                FkTFk.invert();
                Scalar factor = 0.0;
                for (size_t i = 0; i <= mk; ++i)
                    for (size_t j = 0; j <= mk; ++j)
                        factor += FkTFk[i][j];

                Dune::DynamicVector<Scalar> alpha(mk+1);
                for (size_t i = 0; i <= mk; ++i)
                    alpha[i] = std::accumulate(FkTFk[i].begin(), FkTFk[i].end(), 0.0);
                alpha /= factor;

                xi[k+1] = 0;
                for (size_t i = 0; i <= mk; ++i)
                {
                    auto aGki = Gk[k-mk+i];
                    aGki *= alpha[i];
                    xi[k+1] += aGki;
                }
            }
        }

        return false;
    }

    std::string name() const
    {
        return "Uzawa solver";
    }
};

template <class GridGeometry>
class UzawaBiCGSTABBackend : public LinearSolver
{
public:
    using LinearSolver::LinearSolver;

    // precondBlockLevel: set this to more than one if the matrix to solve is nested multiple times
    template<int precondBlockLevel = 1, class Matrix, class Vector>
    bool solve(const Matrix& A, Vector& x, const Vector& b)
    {
        using Preconditioner = SeqUzawa<Matrix, Vector, Vector, GridGeometry>;
        using Solver = Dune::BiCGSTABSolver<Vector>;

        return IterativePreconditionedSolverImpl::template solve<Preconditioner, Solver>(*this, A, x, b, this->paramGroup());
    }

    std::string name() const
    {
        return "Uzawa preconditioned BiCGSTAB solver";
    }
};
// #endif // HAVE_UMFPACK

// \}

} // end namespace Dumux

#endif
