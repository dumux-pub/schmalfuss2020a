// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Linear
 * \brief A helper classe that converts a Dune::MultiTypeBlockMatrix into a plain Dune::BCRSMatrix
 */
#ifndef DUMUX_MATRIX_CONVERTER_XT
#define DUMUX_MATRIX_CONVERTER_XT

#include <dumux/linear/matrixconverter.hh>

#include <cmath>
#include <dune/common/indices.hh>
#include <dune/common/hybridutilities.hh>
/// Begin Jenny
#include <dumux/common/parameters.hh>
// End Jenny
#include <dune/istl/bvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/istl/multitypeblockmatrix.hh>

namespace Dumux {

/*!
 * \ingroup Linear
 * \brief A helper classe that converts a Dune::MultiTypeBlockMatrix into a plain Dune::BCRSMatrix
 * TODO: allow block sizes for BCRSMatrix other than 1x1 ?
 *
 */
template <class MultiTypeBlockMatrix, class Scalar=double>
class MatrixConverterExt : public MatrixConverter<MultiTypeBlockMatrix, Scalar>
{
    using MatrixBlock = typename Dune::FieldMatrix<Scalar, 1, 1>;
    using BCRSMatrix = typename Dune::BCRSMatrix<MatrixBlock>;
    using BcrsBcrsMatrix = typename Dune::BCRSMatrix<BCRSMatrix>;

public:

        /*!
     * \brief Converts the matrix to Dune::BCRSMatrix<Dune::BCRSMatrix>>, to allow processing with the istl::BlockyPreconditioner
     *
     * \param A The original multitype blockmatrix
     */
    static auto multiTypeToBcrsBcrsMatrix(const MultiTypeBlockMatrix &A)
    {
        // get the size for the converted matrix
        const auto numRows = A.N();
        // std::cout << "Create BCRSBCRS Matrix with dims " << numRows << std::endl;

        // create an empty BCRS matrix with 1x1 blocks
        BcrsBcrsMatrix M(numRows, numRows, BcrsBcrsMatrix::random);
        for(int i=0; i<numRows; ++i)
            M.setrowsize(i,numRows);
        M.endrowsizes();

        for(int i=0; i<numRows; ++i){
            for(int j=0; j<numRows; ++j){
                M.addindex(i,j);
            }
        }
        M.endindices();

        // std::cout << "Before create submatrix" << std::endl;

        // fill sub-bcrsmatrices (after setting occupation pattern)

        auto createSubmatrix = [&M, &numRows](const auto& subMatrix, const std::size_t rowIdx, const std::size_t colIdx)
        {
            // std::cout << "Entering create submatrix" << std::endl;

            // using BlockType = typename std::decay_t<decltype(subMatrix)>::block_type;
            using BlockType = typename Dune::FieldMatrix<Scalar, 1, 1>;
            const auto blockSizeI = BlockType::rows;
            const auto blockSizeJ = BlockType::cols;

            // std::cout << "Block sizes I/J: " << blockSizeI << " " << blockSizeJ << std::endl;

            Dune::MatrixIndexSet occupationPattern;
            // !! Blocksize is size of subblock (mostly 1), matblockdim the number of these subblocks per submatrix.
            auto matblockdim_row = subMatrix.N();
            auto matblockdim_col = subMatrix.M(); 
            // std::cout << "Matblockdim row/col: " << matblockdim_row << " " << matblockdim_col << std::endl;
            // std::cout << "subMatrix.N()  " << subMatrix.N() << std::endl;
            // std::cout << "subMatrix.M()  " << subMatrix.M()  << std::endl;
            occupationPattern.resize(blockSizeI*matblockdim_row, blockSizeJ*matblockdim_col); 

            // set ocupation pattern
            using std::abs;
            static const Scalar eps = getParam<Scalar>("MatrixConverter.DeletePatternEntriesBelowAbsThreshold", -1.0);

            // std::cout << "...Create Submatrix: occupation pattern begin" << std::endl;
            for(auto row = subMatrix.begin(); row != subMatrix.end(); ++row)
                for(auto col = row->begin(); col != row->end(); ++col)
                    for(std::size_t i = 0; i < blockSizeI; ++i)
                        for(std::size_t j = 0; j < blockSizeJ; ++j)
                            if(abs(subMatrix[row.index()][col.index()][i][j]) > eps)
                                occupationPattern.add(row.index()*blockSizeI + i, col.index()*blockSizeJ + j);
            // std::cout << "...Create Submatrix: occupation pattern end" << std::endl;

            auto subBCRS = BCRSMatrix(blockSizeI*matblockdim_row, blockSizeJ*matblockdim_col, BCRSMatrix::random);
            occupationPattern.exportIdx(subBCRS);

            // copy values
            // std::cout << "...Create Submatrix: copy values begin" << std::endl;
            for (auto row = subMatrix.begin(); row != subMatrix.end(); ++row)
                for (auto col = row->begin(); col != row->end(); ++col)
                    for (std::size_t i = 0; i < blockSizeI; ++i)
                        for (std::size_t j = 0; j < blockSizeJ; ++j)
                            if(abs(subMatrix[row.index()][col.index()][i][j]) > eps)
                                subBCRS[row.index()*blockSizeI + i][col.index()*blockSizeJ + j] = subMatrix[row.index()][col.index()][i][j];
            // std::cout << "...Create Submatrix: copy values end" << std::endl;
            
            M[rowIdx][colIdx] = subBCRS;
        };

        std::size_t rowIndex = 0;
        Dune::Hybrid::forEach(A, [&createSubmatrix, &rowIndex, numRows](const auto& rowOfMultiTypeMatrix)
        {
            // std::cout << "create submatrix for row " << rowIndex << std::endl;

            std::size_t colIndex = 0;
            Dune::Hybrid::forEach(rowOfMultiTypeMatrix, [&createSubmatrix, &colIndex, &rowIndex, numRows](const auto& subMatrix)
            {
                // std::cout << "create submatrix for col " << colIndex << std::endl;
                createSubmatrix(subMatrix, rowIndex, colIndex);
                colIndex += 1;
                // if we have arrived at the right side of the matrix, increase the row index
                if(colIndex == numRows)
                    rowIndex += 1;
            });
        });

        return M;
    }
};

/*!
 * \ingroup Linear
 * \brief A helper classe that converts a Dune::MultiTypeBlockVector into a plain Dune::BlockVector and transfers back values
 */
template<class MultiTypeBlockVector, class Scalar=double>
class VectorConverterExt : VectorConverter<MultiTypeBlockVector, Scalar>
{
    using VectorBlock = typename Dune::FieldVector<Scalar, 1>;
    using BlockVector = typename Dune::BlockVector<VectorBlock>;
    using BlockBlockVector = typename Dune::BlockVector<BlockVector>;

public:
        /*!
     * \brief Converts a Dune::MultiTypeBlockVector to a plain 1x1 Dune::BlockVector
     *
     * \param b The original multitype blockvector
     */
    static auto multiTypeToBlockBlockVector(const MultiTypeBlockVector& b)
    {
        const auto size = b.size();
        std::cout << "BlockBlockvector-Size: " << size << std::endl;
        BlockBlockVector bbv(size);

        std::size_t startIndex = 0;
        Dune::Hybrid::forEach(b, [&bbv, &startIndex](const auto& subVector)
        {
            const auto numEq = std::decay_t<decltype(subVector)>::block_type::size();
            std::size_t size = numEq * subVector.size();
            BlockVector newSubVec(size);

            for(std::size_t i = 0; i < subVector.size(); ++i)
                for(std::size_t j = 0; j < numEq; ++j)
                    newSubVec[i*numEq + j] = subVector[i][j];

            bbv[startIndex] = newSubVec;
            startIndex += 1;
            // startIndex += numEq*subVector.size();
        });

        return bbv;
    }
};

} // end namespace Dumux

#endif
