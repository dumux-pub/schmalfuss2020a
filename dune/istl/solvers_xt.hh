// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_ISTL_SOLVERS_HH_XT
#define DUNE_ISTL_SOLVERS_HH_XT

#include <dune/istl/solvers.hh>
#include <fstream>

#include <dune/istl/preconditioner_xt.hh>

namespace Dune {

  template<class X, class Y=X, class F = Y>
  class RestartedGMResSolverXT : public IterativeSolver<X,Y>
  {
  public:
    using typename IterativeSolver<X,Y>::domain_type;
    using typename IterativeSolver<X,Y>::range_type;
    using typename IterativeSolver<X,Y>::field_type;
    using typename IterativeSolver<X,Y>::real_type;

  protected:
    using typename IterativeSolver<X,X>::scalar_real_type;

    //! \brief field_type Allocator retrieved from domain type
    using fAlloc = ReboundAllocatorType<X,field_type>;
    //! \brief real_type Allocator retrieved from domain type
    using rAlloc = ReboundAllocatorType<X,real_type>;

  public:

    /*!
       \brief Set up RestartedGMResSolverXT solver.

       \copydoc LoopSolver::LoopSolver(L&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    RestartedGMResSolverXT (LinearOperator<X,Y>& op, Preconditioner<X,Y>& prec, scalar_real_type reduction, int restart, int maxit, int verbose) :
      IterativeSolver<X,Y>::IterativeSolver(op,prec,reduction,maxit,verbose),
      _restart(restart)
    {}

        /*!
       \brief Set up RestartedGMResSolverXT solver.

       \copydoc LoopSolver::LoopSolver(L&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    RestartedGMResSolverXT (LinearOperator<X,Y>& op, Preconditioner<X,Y>& prec, scalar_real_type reduction, int restart, int maxit, int verbose, bool writeres, bool realresbound) :
      IterativeSolver<X,Y>::IterativeSolver(op,prec,reduction,maxit,verbose),
      _restart(restart), _writeres(writeres), _realresbound(realresbound)
    {}

    /*!
       \brief Set up RestartedGMResSolverXT solver.

       \copydoc LoopSolver::LoopSolver(L&,S&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    RestartedGMResSolverXT (LinearOperator<X,Y>& op, ScalarProduct<X>& sp, Preconditioner<X,Y>& prec, scalar_real_type reduction, int restart, int maxit, int verbose) :
      IterativeSolver<X,Y>::IterativeSolver(op,sp,prec,reduction,maxit,verbose),
      _restart(restart)
    {}

        /*!
       \brief Set up RestartedGMResSolverXT solver with residual outfile.

       \copydoc LoopSolver::LoopSolver(L&,S&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    RestartedGMResSolverXT (LinearOperator<X,Y>& op, ScalarProduct<X>& sp, Preconditioner<X,Y>& prec, scalar_real_type reduction, int restart, int maxit, int verbose, bool writeres) :
      IterativeSolver<X,Y>::IterativeSolver(op,sp,prec,reduction,maxit,verbose),
      _restart(restart), _writeres(writeres)
    {}


            /*!
       \brief Set up RestartedGMResSolverXT solver with residual outfile.

       \copydoc LoopSolver::LoopSolver(L&,S&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    RestartedGMResSolverXT (LinearOperator<X,Y>& op, ScalarProduct<X>& sp, Preconditioner<X,Y>& prec, scalar_real_type reduction, int restart, int maxit, int verbose, bool writeres, bool realresbound) :
      IterativeSolver<X,Y>::IterativeSolver(op,sp,prec,reduction,maxit,verbose),
      _restart(restart), _writeres(writeres), _realresbound(realresbound)
    {}

    /*!
       \brief Apply inverse operator.

       \copydoc InverseOperator::apply(X&,Y&,InverseOperatorResult&)

       \note Currently, the RestartedGMResSolverXT aborts when it detects a
             breakdown.
     */
    virtual void apply (X& x, Y& b, InverseOperatorResult& res)
    {
      apply(x,b,Simd::max(_reduction),res);
    }

    /*!
       \brief Apply inverse operator.

       \copydoc InverseOperator::apply(X&,Y&,double,InverseOperatorResult&)

       \note Currently, the RestartedGMResSolverXT aborts when it detects a
             breakdown.
     */
    virtual void apply (X& x, Y& b, double reduction, InverseOperatorResult& res)
    {
      using std::abs;
      const Simd::Scalar<real_type> EPSILON = 1e-80;
      const int m = _restart;
      real_type norm, norm_old = 0.0, norm_0;
      real_type norm_realres, norm_old_realres = 0.0, norm_0_realres;
      int j = 1;
      std::vector<field_type,fAlloc> s(m+1), sn(m);
      std::vector<real_type,rAlloc> cs(m);
      // need copy of rhs if GMRes has to be restarted
      Y b2(b);
      // helper vector
      Y w(b);
      std::vector< std::vector<field_type,fAlloc> > H(m+1,s);
      std::vector<F> v(m+1,b);

      // start timer
      Dune::Timer watch;
      watch.reset();

      // clear solver statistics and set res.converged to false
      res.clear();
      _prec->pre(x,b);

      // calculate defect and overwrite rhs with it
      _op->applyscaleadd(-1.0,x,b); // b -= Ax

      // write initial residual to output
      if (_writeres){
            writetofile(b,0);
      }

      // calculate preconditioned defect
      v[0] = 0.0; _prec->apply(v[0],b); // r = W^-1 b
      norm_0 = _sp->norm(v[0]);
      if (_realresbound){
        norm_0_realres = _sp->norm(b);
        norm_realres = norm_0_realres;
        norm_old_realres = norm_realres;
      }
      norm = norm_0;
      norm_old = norm;

      // print header
      if(_verbose > 0)
        {
          std::cout << "=== RestartedGMResSolverXT" << std::endl;
          if(_verbose > 1) {
            this->printHeader(std::cout);
            this->printOutput(std::cout,0,norm_0);
          }
        }

      if(Simd::allTrue(norm_0 < EPSILON))
      {
        res.converged = true;
        if(_verbose > 0) // final print
          print_result(res);
      }

      while(j <= _maxit && res.converged != true) {

        int i = 0;
        v[0] *= real_type(1.0)/norm;
        s[0] = norm;
        for(i=1; i<m+1; i++)
          s[i] = 0.0;

        for(i=0; i < m && j <= _maxit && res.converged != true; i++, j++) {
          w = 0.0;
          // use v[i+1] as temporary vector
          v[i+1] = 0.0;
          // do Arnoldi algorithm
          _op->apply(v[i],v[i+1]);
          _prec->apply(w,v[i+1]);
          for(int k=0; k<i+1; k++) {
            // notice that _sp->dot(v[k],w) = v[k]\adjoint w
            // so one has to pay attention to the order
            // in the scalar product for the complex case
            // doing the modified Gram-Schmidt algorithm
            H[k][i] = _sp->dot(v[k],w);
            // w -= H[k][i] * v[k]
            w.axpy(-H[k][i],v[k]);
          }
          H[i+1][i] = _sp->norm(w);
          if(Simd::allTrue(abs(H[i+1][i]) < EPSILON))
            DUNE_THROW(SolverAbort,
                       "breakdown in GMRes - |w| == 0.0 after " << j << " iterations");

          // normalize new vector
          v[i+1] = w; v[i+1] *= real_type(1.0)/H[i+1][i];

          // update QR factorization
          for(int k=0; k<i; k++)
            applyPlaneRotation(H[k][i],H[k+1][i],cs[k],sn[k]);

          // compute new givens rotation
          generatePlaneRotation(H[i][i],H[i+1][i],cs[i],sn[i]);
          // finish updating QR factorization
          applyPlaneRotation(H[i][i],H[i+1][i],cs[i],sn[i]);
          applyPlaneRotation(s[i],s[i+1],cs[i],sn[i]);

          // norm of the defect is the last component the vector s
          norm = abs(s[i+1]);

          // print current iteration statistics
          if(_verbose > 1) {
            this->printOutput(std::cout,j,norm,norm_old);
          }

          norm_old = norm;

          // check convergence
          if (! _realresbound){
            if(Simd::allTrue(norm < real_type(reduction) * norm_0))
              res.converged = true;
          }

        } // end for

        // calculate update vector
        w = 0.0;
        update(w,i,H,s,v);
        // and current iterate
        x += w;

        // restart GMRes if convergence was not achieved,
        // i.e. linear defect has not reached desired reduction
        // and if j < _maxit (do not restart on last iteration)
        if( res.converged != true && j < _maxit ) {

          if(_verbose > 0)
            std::cout << "=== GMRes::restart" << std::endl;
          // get saved rhs
          b = b2;
          // calculate new defect
          _op->applyscaleadd(-1.0,x,b); // b -= Ax;
          if (_writeres){
            writetofile(b,j-1);
          }
          // calculate preconditioned defect -> this doesn't change the residual b, as apply takes const b.
          v[0] = 0.0;
          _prec->apply(v[0],b);

          if (_realresbound){
            norm_realres = _sp->norm(b);
            norm_old_realres = norm_realres;
            if (norm_realres <= real_type(reduction) )
              res.converged = true;
          }
          norm_old = norm;


        }

        if (_writeres && (res.converged || j>= _maxit) ){
          b = b2;
          // calculate new defect
          _op->applyscaleadd(-1.0,x,b); // b -= Ax;
          if (_writeres){
            writetofile(b,j-1);
          }
        }

      } //end while

      // postprocess preconditioner
      _prec->post(x);

      // save solver statistics
      res.iterations = j-1; // it has to be j-1!!!
      res.reduction = static_cast<double>(Simd::max(norm/norm_0));
      res.conv_rate = pow(res.reduction,1.0/(j-1));
      res.elapsed = watch.elapsed();

      if(_verbose>0)
        print_result(res);

    }

    void update_outfile(std::string outfile){
      _outfile = outfile;
      _resfileopened = false;
    }

  protected :

    void print_result(const InverseOperatorResult& res) const {
      int k = res.iterations>0 ? res.iterations : 1;
      std::cout << "=== rate=" << res.conv_rate
                << ", T=" << res.elapsed
                << ", TIT=" << res.elapsed/k
                << ", IT=" << res.iterations
                << std::endl;
    }

    void update(X& w, int i,
                const std::vector<std::vector<field_type,fAlloc> >& H,
                const std::vector<field_type,fAlloc>& s,
                const std::vector<X>& v) {
      // solution vector of the upper triangular system
      std::vector<field_type,fAlloc> y(s);

      // backsolve
      for(int a=i-1; a>=0; a--) {
        field_type rhs(s[a]);
        for(int b=a+1; b<i; b++)
          rhs -= H[a][b]*y[b];
        y[a] = rhs/H[a][a];

        // compute update on the fly
        // w += y[a]*v[a]
        w.axpy(y[a],v[a]);
      }
    }

    void writetofile(Y& b, int j){

      std::ofstream resfile;
      std::string residualfilename = _outfile;

      // Initial Opening of File, write Header
      if (_resfileopened != true){
        resfile.open(residualfilename, std::ios::out );
        // Format: 
        // #Subvectors | NameOf1stMeasure | NameOf2ndMeasure ...
        resfile << b.N() << " " << "two_norm" << " " << "infty_norm";
        resfile << "\n";
        resfile.close();
        _resfileopened = true;
      }

      // = 2-norm
      float twonormvec = _sp->norm(b);
      float infty = 0;
      for (int i=0; i<b.N(); i++){
        float inftyb = b[i].infinity_norm();
        // float twonormb = b[i].two_norm();
        if (inftyb > infty)
          infty = inftyb;
        // std::cout << twonormb <<  " " << inftyb << std::endl;
      }
      // std::cout << twonormvec << " " << infty << std::endl;
      // Dune::printvector(std::cout, b[0], "", "");

      // Fill file with residual data with format
      // timepoint | Measures-For-Whole-Vector | Measures-per-Subvectors
      // timePoint | 1stMeasWholeVec | 2ndMeasWholeVec | 1stMeasVec_0 | 2ndMeasVec_0 | 1stMeasVec_1 | 2ndMeasVec_1 | ....
      resfile.open(residualfilename, std::ios::app);
      resfile << j;
      resfile << " " << twonormvec;
      resfile << " " << infty;
      for (int i=0; i<b.N(); i++){
        resfile << " " << b[i].two_norm();
        resfile << " " << b[i].infinity_norm();
      }
      resfile << "\n";
      resfile.close();
    }

    template<typename T>
    typename std::enable_if<std::is_same<field_type,real_type>::value,T>::type conjugate(const T& t) {
      return t;
    }

    template<typename T>
    typename std::enable_if<!std::is_same<field_type,real_type>::value,T>::type conjugate(const T& t) {
      using std::conj;
      return conj(t);
    }

    void
    generatePlaneRotation(field_type &dx, field_type &dy, real_type &cs, field_type &sn)
    {
      using std::sqrt;
      using std::abs;
      using std::max;
      using std::min;
      const real_type eps = 1e-15;
      real_type norm_dx = abs(dx);
      real_type norm_dy = abs(dy);
      real_type norm_max = max(norm_dx, norm_dy);
      real_type norm_min = min(norm_dx, norm_dy);
      real_type temp = norm_min/norm_max;
      // we rewrite the code in a vectorizable fashion
      cs = Simd::cond(norm_dy < eps,
        real_type(1.0),
        Simd::cond(norm_dx < eps,
          real_type(0.0),
          Simd::cond(norm_dy > norm_dx,
            real_type(1.0)/sqrt(real_type(1.0) + temp*temp)*temp,
            real_type(1.0)/sqrt(real_type(1.0) + temp*temp)
          )));
      sn = Simd::cond(norm_dy < eps,
        field_type(0.0),
        Simd::cond(norm_dx < eps,
          field_type(1.0),
          Simd::cond(norm_dy > norm_dx,
            field_type(1.0)/sqrt(real_type(1.0) + temp*temp)*dx*conjugate(dy)/norm_dx/norm_dy,
            field_type(1.0)/sqrt(real_type(1.0) + temp*temp)*conjugate(dy/dx)
          )));
    }


    void
    applyPlaneRotation(field_type &dx, field_type &dy, real_type &cs, field_type &sn)
    {
      field_type temp  =  cs * dx + sn * dy;
      dy = -conjugate(sn) * dx + cs * dy;
      dx = temp;
    }

    using IterativeSolver<X,Y>::_op;
    using IterativeSolver<X,Y>::_prec;
    using IterativeSolver<X,Y>::_sp;
    using IterativeSolver<X,Y>::_reduction;
    using IterativeSolver<X,Y>::_maxit;
    using IterativeSolver<X,Y>::_verbose;
    int _restart;
    bool _writeres = false;
    bool _resfileopened = false;
    bool _realresbound = false;
    std::string _outfile = "GMRes-M_residuals.txt";
  };



/**
     \brief implements the Proportional Derivative Minimal Residual (PD-GMRes) method

     GMRes solves the unsymmetric linear system Ax = b using the
     Generalized Minimal Residual method as described the SIAM Templates
     book (http://www.netlib.org/templates/templates.pdf).

     \tparam X trial vector, vector type of the solution
     \tparam Y test vector, vector type of the RHS
     \tparam F vector type for orthonormal basis of Krylov space

   */

  template<class X, class Y=X, class F = Y>
  class PDGMResSolver : public IterativeSolver<X,Y>
  {
  public:
    using typename IterativeSolver<X,Y>::domain_type;
    using typename IterativeSolver<X,Y>::range_type;
    using typename IterativeSolver<X,Y>::field_type;
    using typename IterativeSolver<X,Y>::real_type;

  protected:
    using typename IterativeSolver<X,X>::scalar_real_type;

    //! \brief field_type Allocator retrieved from domain type
    using fAlloc = ReboundAllocatorType<X,field_type>;
    //! \brief real_type Allocator retrieved from domain type
    using rAlloc = ReboundAllocatorType<X,real_type>;

  public:

    /*!
       \brief Set up PDGMResSolver solver.

       \copydoc LoopSolver::LoopSolver(L&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    PDGMResSolver (LinearOperator<X,Y>& op, Preconditioner<X,Y>& prec, scalar_real_type reduction, int m_init, int m_min, int m_step, int maxit, int verbose) :
      IterativeSolver<X,Y>::IterativeSolver(op,prec,reduction,maxit,verbose),
      _m_init(m_init), _m_min{m_min}, _m_step{m_step}
    {}

        /*!
       \brief Set up PDGMResSolver solver.

       \copydoc LoopSolver::LoopSolver(L&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    PDGMResSolver (LinearOperator<X,Y>& op, Preconditioner<X,Y>& prec, scalar_real_type reduction, int m_init, int m_min, int m_step, int maxit, int verbose, bool writeres, bool realresbound) :
      IterativeSolver<X,Y>::IterativeSolver(op,prec,reduction,maxit,verbose),
      _m_init(m_init), _m_min{m_min}, _m_step{m_step}, _writeres(writeres), _realresbound(realresbound)
    {}

    /*!
       \brief Set up PDGMResSolver solver.

       \copydoc LoopSolver::LoopSolver(L&,S&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    PDGMResSolver (LinearOperator<X,Y>& op, ScalarProduct<X>& sp, Preconditioner<X,Y>& prec, scalar_real_type reduction, int m_init, int m_min, int m_step, int maxit, int verbose) :
      IterativeSolver<X,Y>::IterativeSolver(op,sp,prec,reduction,maxit,verbose),
      _m_init(m_init), _m_min{m_min}, _m_step{m_step}
    {}

        /*!
       \brief Set up PDGMResSolver solver with residual outfile.

       \copydoc LoopSolver::LoopSolver(L&,S&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    PDGMResSolver (LinearOperator<X,Y>& op, ScalarProduct<X>& sp, Preconditioner<X,Y>& prec, scalar_real_type reduction, int m_init, int m_min, int m_step, int maxit, int verbose, bool writeres) :
      IterativeSolver<X,Y>::IterativeSolver(op,sp,prec,reduction,maxit,verbose),
      _m_init(m_init), _m_min{m_min}, _m_step{m_step}, _writeres(writeres)
    {}


            /*!
       \brief Set up PDGMResSolver solver with residual outfile.

       \copydoc LoopSolver::LoopSolver(L&,S&,P&,double,int,int)
       \param restart number of GMRes cycles before restart
     */
    PDGMResSolver (LinearOperator<X,Y>& op, ScalarProduct<X>& sp, Preconditioner<X,Y>& prec, scalar_real_type reduction, int m_init, int m_min, int m_step, int maxit, int verbose, bool writeres, bool realresbound) :
      IterativeSolver<X,Y>::IterativeSolver(op,sp,prec,reduction,maxit,verbose),
      _m_init(m_init), _m_min{m_min}, _m_step{m_step}, _writeres(writeres), _realresbound(realresbound)
    {}

    /*!
       \brief Apply inverse operator.

       \copydoc InverseOperator::apply(X&,Y&,InverseOperatorResult&)

       \note Currently, the PDGMResSolver aborts when it detects a
             breakdown.
     */
    virtual void apply (X& x, Y& b, InverseOperatorResult& res)
    {
      apply(x,b,Simd::max(_reduction),res);
    }

    /*!
       \brief Apply inverse operator.

       \copydoc InverseOperator::apply(X&,Y&,double,InverseOperatorResult&)

       \note Currently, the PDGMResSolver aborts when it detects a
             breakdown.
     */
    virtual void apply (X& x, Y& b, double reduction, InverseOperatorResult& res)
    {
      using std::abs;
      const Simd::Scalar<real_type> EPSILON = 1e-80;
      real_type norm, norm_old = 0.0, norm_0;
      real_type norm_realres, norm_0_realres;
      int j = 1;
      // need copy of rhs if GMRes has to be restarted
      Y b2(b);
      // helper vector
      Y w(b);

      // start timer
      Dune::Timer watch;
      watch.reset();
      std::cout << "ENTERING TIMER" << std::endl;

      // clear solver statistics and set res.converged to false
      res.clear();
      _prec->pre(x,b);

      // calculate defect and overwrite rhs with it
      _op->applyscaleadd(-1.0,x,b); // b -= Ax
      _r_j_1 = _sp->norm(b);
      _r_j_2 = _r_j_1;
      _r_j_3 = _r_j_1;

      // PD-GMRes for j=1 (initialized in private variables):
      if (_m_init < _m_min){
        _m_init += _m_step;
        _m_j = _m_init;
      }
      else{
        _m_j = _m_init;
      }

      if (_realresbound){
        norm_0_realres = _sp->norm(b);
        norm_realres = norm_0_realres;
      }
      // write initial residual to output
      if (_writeres){
            writetofile(b,0);
      }


      // print header
      if(_verbose > 0)
        {
          std::cout << "=== PDGMResSolver" << std::endl;
          if(_verbose > 1) {
            this->printHeader(std::cout);
            // this->printOutput(std::cout,0,norm_0);
          }
        }


      while(j <= _maxit && res.converged != true) {

        std::vector<field_type,fAlloc> s(_m_j+1), sn(_m_j);
        std::vector<real_type,rAlloc> cs(_m_j);
        std::vector< std::vector<field_type,fAlloc> > H(_m_j+1,s);
        std::vector<F> v(_m_j+1,b);

        v[0] = 0.0; 
        _prec->apply(v[0],b); // r = W^-1 b
        norm_0 = _sp->norm(v[0]);

        if (j==1){
          // calculate preconditioned defect
          norm = norm_0;
          norm_old = norm;

          if(Simd::allTrue(norm_0 < EPSILON))
          {
            res.converged = true;
            if(_verbose > 0) // final print
              print_result(res);
          }
        }


        int i = 0;
        v[0] *= real_type(1.0)/norm;
        s[0] = norm;
        for(i=1; i<_m_j+1; i++)
          s[i] = 0.0;

        for(i=0; i < _m_j && j <= _maxit && res.converged != true; i++, j++) {
          w = 0.0;
          // use v[i+1] as temporary vector
          v[i+1] = 0.0;
          // do Arnoldi algorithm
          _op->apply(v[i],v[i+1]);
          _prec->apply(w,v[i+1]);
          for(int k=0; k<i+1; k++) {
            // notice that _sp->dot(v[k],w) = v[k]\adjoint w
            // so one has to pay attention to the order
            // in the scalar product for the complex case
            // doing the modified Gram-Schmidt algorithm
            H[k][i] = _sp->dot(v[k],w);
            // w -= H[k][i] * v[k]
            w.axpy(-H[k][i],v[k]);
          }
          H[i+1][i] = _sp->norm(w);
          if(Simd::allTrue(abs(H[i+1][i]) < EPSILON))
            DUNE_THROW(SolverAbort,
                       "breakdown in GMRes - |w| == 0.0 after " << j << " iterations");

          // normalize new vector
          v[i+1] = w; v[i+1] *= real_type(1.0)/H[i+1][i];

          // update QR factorization
          for(int k=0; k<i; k++)
            applyPlaneRotation(H[k][i],H[k+1][i],cs[k],sn[k]);

          // compute new givens rotation
          generatePlaneRotation(H[i][i],H[i+1][i],cs[i],sn[i]);
          // finish updating QR factorization
          applyPlaneRotation(H[i][i],H[i+1][i],cs[i],sn[i]);
          applyPlaneRotation(s[i],s[i+1],cs[i],sn[i]);

          // norm of the defect is the last component the vector s
          norm = abs(s[i+1]);

          // print current iteration statistics
          if(_verbose > 1) {
            this->printOutput(std::cout,j,norm,norm_old);
          }

          norm_old = norm;

          // check convergence
          if (! _realresbound){
            if(Simd::allTrue(norm < real_type(reduction) * norm_0))
              res.converged = true;
          }

        } // end for

        // calculate update vector
        w = 0.0;
        update(w,i,H,s,v);
        // and current iterate
        x += w;

        // restart GMRes if convergence was not achieved,
        // i.e. linear defect has not reached desired reduction
        // and if j < _maxit (do not restart on last iteration)
        if( res.converged != true && j < _maxit ) {

          // get saved rhs
          b = b2;
          // calculate new defect
          _op->applyscaleadd(-1.0,x,b); // b -= Ax;
          if (_writeres){
            writetofile(b,j-1);
          }
          // // calculate preconditioned defect
          // v[0] = 0.0;
          // _prec->apply(v[0],b);

          double norm_resid = _sp->norm(b);

          if (_realresbound){
            norm_realres = norm_resid;
            if (norm_realres <= real_type(reduction) )
              res.converged = true;
          }
          norm_old = norm;

          // PD-GMRES
          _j += 1;
          _r_j_3 = _r_j_2;
          _r_j_2 = _r_j_1;
          _r_j_1 = norm_resid;
          if (_j > 3)
            _m_j += floor(-3.0 * _r_j_1 / _r_j_2   +   5.0/2.0 * (_r_j_1 - _r_j_3) / _r_j_2);
          else if (_j > 2)
            _m_j += floor(-3.0 * _r_j_1 / _r_j_2);
          else
            _m_j = _m_init;

          if (_m_j < _m_min){
            _m_init += _m_step;
            _m_j = _m_init;
          }

          if(_verbose > 0)
            std::cout << "=== GMRes::restart with m=" << _m_j << std::endl;


        }

        if (_writeres && (res.converged || j>= _maxit) ){
          b = b2;
          // calculate new defect
          _op->applyscaleadd(-1.0,x,b); // b -= Ax;
          if (_writeres){
            writetofile(b,j-1);
          }
        }

      } //end while

      // postprocess preconditioner
      _prec->post(x);

      // save solver statistics
      res.iterations = j-1; // it has to be j-1!!!
      res.reduction = static_cast<double>(Simd::max(norm/norm_0));
      res.conv_rate = pow(res.reduction,1.0/(j-1));
      res.elapsed = watch.elapsed();

      if(_verbose>0)
        print_result(res);

    }

    void update_outfile(std::string outfile){
      _outfile = outfile;
      _resfileopened = false;
    }

  protected :

    void print_result(const InverseOperatorResult& res) const {
      int k = res.iterations>0 ? res.iterations : 1;
      std::cout << "=== rate=" << res.conv_rate
                << ", T=" << res.elapsed
                << ", TIT=" << res.elapsed/k
                << ", IT=" << res.iterations
                << std::endl;
    }

    void update(X& w, int i,
                const std::vector<std::vector<field_type,fAlloc> >& H,
                const std::vector<field_type,fAlloc>& s,
                const std::vector<X>& v) {
      // solution vector of the upper triangular system
      std::vector<field_type,fAlloc> y(s);

      // backsolve
      for(int a=i-1; a>=0; a--) {
        field_type rhs(s[a]);
        for(int b=a+1; b<i; b++)
          rhs -= H[a][b]*y[b];
        y[a] = rhs/H[a][a];

        // compute update on the fly
        // w += y[a]*v[a]
        w.axpy(y[a],v[a]);
      }
    }

    void writetofile(Y& b, int j){

      std::ofstream resfile;
      std::string residualfilename = _outfile;

      // Initial Opening of File, write Header
      if (_resfileopened != true){
        resfile.open(residualfilename, std::ios::out );
        // Format: 
        // #Subvectors | NameOf1stMeasure | NameOf2ndMeasure ...
        resfile << b.N() << " " << "two_norm" << " " << "infty_norm";
        resfile << "\n";
        resfile.close();
        _resfileopened = true;
      }

      // = 2-norm
      float twonormvec = _sp->norm(b);
      float infty = 0;
      for (int i=0; i<b.N(); i++){
        float inftyb = b[i].infinity_norm();
        // float twonormb = b[i].two_norm();
        if (inftyb > infty)
          infty = inftyb;
        // std::cout << twonormb <<  " " << inftyb << std::endl;
      }
      // std::cout << twonormvec << " " << infty << std::endl;
      // Dune::printvector(std::cout, b[0], "", "");

      // Fill file with residual data with format
      // timepoint | Measures-For-Whole-Vector | Measures-per-Subvectors
      // timePoint | 1stMeasWholeVec | 2ndMeasWholeVec | 1stMeasVec_0 | 2ndMeasVec_0 | 1stMeasVec_1 | 2ndMeasVec_1 | ....
      resfile.open(residualfilename, std::ios::app);
      resfile << j;
      resfile << " " << twonormvec;
      resfile << " " << infty;
      for (int i=0; i<b.N(); i++){
        resfile << " " << b[i].two_norm();
        resfile << " " << b[i].infinity_norm();
      }
      resfile << "\n";
      resfile.close();
    }

    template<typename T>
    typename std::enable_if<std::is_same<field_type,real_type>::value,T>::type conjugate(const T& t) {
      return t;
    }

    template<typename T>
    typename std::enable_if<!std::is_same<field_type,real_type>::value,T>::type conjugate(const T& t) {
      using std::conj;
      return conj(t);
    }

    void
    generatePlaneRotation(field_type &dx, field_type &dy, real_type &cs, field_type &sn)
    {
      using std::sqrt;
      using std::abs;
      using std::max;
      using std::min;
      const real_type eps = 1e-15;
      real_type norm_dx = abs(dx);
      real_type norm_dy = abs(dy);
      real_type norm_max = max(norm_dx, norm_dy);
      real_type norm_min = min(norm_dx, norm_dy);
      real_type temp = norm_min/norm_max;
      // we rewrite the code in a vectorizable fashion
      cs = Simd::cond(norm_dy < eps,
        real_type(1.0),
        Simd::cond(norm_dx < eps,
          real_type(0.0),
          Simd::cond(norm_dy > norm_dx,
            real_type(1.0)/sqrt(real_type(1.0) + temp*temp)*temp,
            real_type(1.0)/sqrt(real_type(1.0) + temp*temp)
          )));
      sn = Simd::cond(norm_dy < eps,
        field_type(0.0),
        Simd::cond(norm_dx < eps,
          field_type(1.0),
          Simd::cond(norm_dy > norm_dx,
            field_type(1.0)/sqrt(real_type(1.0) + temp*temp)*dx*conjugate(dy)/norm_dx/norm_dy,
            field_type(1.0)/sqrt(real_type(1.0) + temp*temp)*conjugate(dy/dx)
          )));
    }


    void
    applyPlaneRotation(field_type &dx, field_type &dy, real_type &cs, field_type &sn)
    {
      field_type temp  =  cs * dx + sn * dy;
      dy = -conjugate(sn) * dx + cs * dy;
      dx = temp;
    }

    using IterativeSolver<X,Y>::_op;
    using IterativeSolver<X,Y>::_prec;
    using IterativeSolver<X,Y>::_sp;
    using IterativeSolver<X,Y>::_reduction;
    int _m_init;
    const int _m_min;
    const int _m_step;
    using IterativeSolver<X,Y>::_maxit;
    using IterativeSolver<X,Y>::_verbose;
    int _m_j;
    int _m_j_1;
    int _j = 1;
    double _r_j_1;
    double _r_j_2;
    double _r_j_3;
    bool _writeres = false;
    bool _resfileopened = false;
    bool _realresbound = false;
    std::string _outfile = "GMRes-M_residuals.txt";
  };


} // end namespace

#endif
