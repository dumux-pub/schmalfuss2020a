// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_ISTL_PRECONDITIONER_HH_XT
#define DUNE_ISTL_PRECONDITIONER_HH_XT

#include <dune/common/exceptions.hh>

#include <dune/istl/solvercategory.hh>

//Jenny
#include <dune/istl/preconditioner.hh>

#include <dumux/common/partial_xt.hh>
// #include <dumux/linear/seqsolverbackend_xt.hh>
#include <dune/istl/preconditioners_xt.hh>
//end Jenny

namespace Dune {

  /*!
     \Sequential Block preconditioner.

     Takes a BCRS matrix of BCRS matrices and applies specified preconditioners to the submatrices.

     \tparam M The matrix type to operate on
     \tparam X Type of the update
     \tparam Y Type of the defect
     \tparam XV Type of the update subvector
     \tparam YV Type of the defect subvector
     \tparam P Matrix of references to preconditioners to apply to blocks of input matrix
   */
  template<class M, class X, class Y, class XV, class YV, class P>
  class BlockyPreconditioner : public Preconditioner<X,Y> {
  public:

    //! \brief The matrix type the preconditioner is for.
    typedef M matrix_type;
    //! \brief The domain type of the preconditioner.
    typedef X domain_type;
    //! \brief The range type of the preconditioner.
    typedef Y range_type;
    //! \brief The domain type of the preconditioner for the subvectors.
    typedef XV sub_domain_type;
    //! \brief The range type of the preconditioner for the subvectors.
    typedef YV sub_range_type;
    //! \brief The matrix type holding the preconditioners.
    typedef P preconditioner_type;
    typedef typename M::ConstRowIterator rowiteratorM;
    typedef typename M::ConstColIterator coliteratorM;
    //! \brief The field type of the preconditioner.
    typedef typename X::field_type field_type;


    /*! \brief Constructor.

       constructor gets all parameters to operate the prec.
       \param A The matrix to operate on.
       \param B The matrix of preconditioners to apply to the matrix.
       \param w The relaxation factor.
     */
    BlockyPreconditioner (const M& A, const P& B)
      : _A_{A}, _B{B}
    {
      //TODO: Check if dimensions match!!!!
    }

    /*!
       \brief Prepare the preconditioner.

       \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b)
    {
      DUNE_UNUSED_PARAMETER(x);
      DUNE_UNUSED_PARAMETER(b);
      typedef typename M::ConstRowIterator rowiteratorM;
      typedef typename X::ConstIterator rowiteratorX;
      typedef typename Y::ConstIterator rowiteratorY;


      rowiteratorM endi=_A_.end();
      int precIDX = 0;
      rowiteratorX ix = x.begin();



      for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      {
        int v_n = (*ix).N();
        XV vi_temp(v_n, v_n); // A zero-vector of the size of v_i

        for (rowiteratorY ib = b.begin(); ib!=b.end(); ib++)
        {
          vi_temp *= 0.0; // make zero to get linear operator from jacobi/AMG preconditioner (initial solution needs to be 0 for this)
          // std::cout << "Initializing (" << i.index() << ", " << ib.index() << ") with A-dims=(" << _A_[i.index()][ib.index()].N() << ", " << _A_[i.index()][ib.index()].M() <<") x=" << (*ix).N() << ", d=" << (*ib).N() << std::endl; 
          Preconditioner<XV, YV> *_prec = _B.at(precIDX);
          _prec->pre(vi_temp, b[ib.index()]);

          ++precIDX;
        }
        ++ix;
      }
    }


    /*!
       \brief Apply the preconditioner.

       Calculate x = M^-1 d

       \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d)
    {
      typedef typename M::ConstRowIterator rowiteratorM;
      typedef typename X::ConstIterator rowiteratorX;
      typedef typename Y::ConstIterator rowiteratorY;


      rowiteratorM endi=_A_.end();
      // std::cout << "Begin apply BlockPreconditioner" << std::endl;
      int precIDX = 0;
      rowiteratorX iv = v.begin();

      for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      {
        int v_n = (*iv).N();
        XV vi_temp(v_n, v_n); // A zero-vector of the size of v_i
        

        for (rowiteratorY id = d.begin(); id!=d.end(); id++)
        {
          XV vi_init(v_n, v_n); // Need a zero vector instead of a copy of v[i], because otherwise iterative preconditioners will depend on initial solution v[i]. To avoid this, the initial solution is set to 0.
          // Do preconditioning
          Preconditioner<XV, YV> *_prec = _B.at(precIDX);
          // std::cout << "Preconditioning (" << iv.index() << ", " << id.index() << ") with A-dims=(" << _A_[iv.index()][id.index()].N() << ", " << _A_[iv.index()][id.index()].M() <<") x=" << (*iv).N() << ", d=" << (*id).N() << std::endl; 
          _prec->apply(vi_init, *id);
          vi_temp += vi_init;
          ++precIDX;
        }
        v[iv.index()] = vi_temp;
        ++iv;
      }
      // std::cout << "End apply BlockyPreconditioner" << std::endl;

    }

    /*!
       \brief Clean up.

       \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x)
    {
      DUNE_UNUSED_PARAMETER(x);

      typedef typename M::ConstRowIterator rowiteratorM;
      typedef typename M::ConstColIterator coliteratorM;
      typedef typename X::ConstIterator rowiteratorX;

      rowiteratorM endi=_A_.end();
      int precIDX = 0;
      rowiteratorX ix = x.begin();

      for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      {
        for (coliteratorM j=(*i).begin(); j!=(*i).end(); ++j)
        {
          Preconditioner<XV, YV> *_prec = _B.at(precIDX);
          _prec->post(x[ix.index()]);
          precIDX += 1;
        }
        ++ix;
      }
    }

        //! Category of the preconditioner (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }

  private:
    //! \brief the matrix we operate on.
    const M& _A_;
    //! \brief the matrix we operate on.
    const P& _B;

  };




  /*!
     \Sequential Block Uzawa preconditioner.

     Takes a BCRS matrix of BCRS matrices and applies specified preconditioners to the submatrices.
     Applying UZAWA does only work if UZAWA index in matrix are p=0 and v=1. 
     See CHANGE_UZAWA for place where the extension to the general case should be implemented

     \tparam M The matrix type to operate on
     \tparam X Type of the update
     \tparam Y Type of the defect
     \tparam XV Type of the update subvector
     \tparam YV Type of the defect subvector
     \tparam P Matrix of references to preconditioners to apply to blocks of input matrix
   */
  template<class M, class X, class Y, class XV, class YV, class P, class SUZW>
  class BlockyUzawaPreconditioner : public Preconditioner<X,Y> {
  public:

    //! \brief The matrix type the preconditioner is for.
    typedef M matrix_type;
    //! \brief The domain type of the preconditioner.
    typedef X domain_type;
    //! \brief The range type of the preconditioner.
    typedef Y range_type;
    //! \brief The domain type of the preconditioner for the subvectors.
    typedef XV sub_domain_type;
    //! \brief The range type of the preconditioner for the subvectors.
    typedef YV sub_range_type;
    //! \brief The matrix type holding the preconditioners.
    typedef P preconditioner_type;
    //! \brief The Grid Geometry for the Uzawa preconditioner.
    typedef SUZW sequzawa_type;
    typedef typename M::ConstRowIterator rowiteratorM;
    typedef typename M::ConstColIterator coliteratorM;
    //! \brief The field type of the preconditioner.
    typedef typename X::field_type field_type;


    /*! \brief Constructor.

       constructor gets all parameters to operate the prec.
       \param A The matrix to operate on.
       \param B The matrix of preconditioners to apply to the matrix.
       \param w The relaxation factor.
     */
    BlockyUzawaPreconditioner (const M& A, const P& B)
      : _A_{A}, _B{B}, hasUZAWA_{false}
    {
      //TODO: Check if dimensions match!!!!
    }

    BlockyUzawaPreconditioner (const M& A, const P& B, const bool hasUZAWA, int uzawa_it, double uzawa_omega)
      : _A_{A}, _B{B}, hasUZAWA_{hasUZAWA}, uzawa_it_{uzawa_it}, uzawa_omega_{uzawa_omega}
    {
      // if(hasUZAWA_){
      //   seqUzawa_ = SUZW(_A_, uzawa_it, uzawa_omega);
      // }
      //TODO: Check if dimensions match!!!!
    }

    /*!
       \brief Prepare the preconditioner.

       \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b)
    {
      DUNE_UNUSED_PARAMETER(x);
      DUNE_UNUSED_PARAMETER(b);
      typedef typename M::ConstRowIterator rowiteratorM;
      typedef typename M::ConstColIterator coliteratorM;
      typedef typename X::ConstIterator rowiteratorX;
      typedef typename Y::ConstIterator rowiteratorY;


      rowiteratorM endi=_A_.end();
      int precIDX = 0;
      rowiteratorX ix = x.begin();
      rowiteratorY ib = b.begin();



      for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      {
        int v_n = (*ix).N();
        XV vi_temp(v_n, v_n); // A zero-vector of the size of v_i
        int colctr = 0;

        for (coliteratorM j=(*i).begin(); j!=(*i).end(); ++j)
        {
          // 1st case: Initialize Uzawa
          if (hasUZAWA_ and ix.index()==0 and ib.index()==0 ){
            v_uzawa = Dumux::partial(x, Dune::index_constant<0>{}, Dune::index_constant<1>{});
            b_uzawa = Dumux::partial(b, Dune::index_constant<0>{}, Dune::index_constant<1>{});
            seqUzawa_.pre(v_uzawa, b_uzawa); 
            x[0] = v_uzawa[0];
            x[1] = v_uzawa[1];
            b[0] = b_uzawa[0];
            b[1] = b_uzawa[1];
          }
          // 2nd case: Initialize everything that is not Uzawa
          else if (hasUZAWA_ and (ix.index()>1 or ib.index()>1)){
            Preconditioner<XV, YV> *_prec = _B.at(precIDX);
            _prec->pre(x[ix.index()], b[ib.index()]);
          }
          // 3rd case: If no uzawa, initialize every precontitioner
          else if (! hasUZAWA_) {
            Preconditioner<XV, YV> *_prec = _B.at(precIDX);
            _prec->pre(x[ix.index()], b[ib.index()]);
          }
          else{}

          ++precIDX;
          ++ib;
          ++colctr;
        }
        ++ix;
        ib -= colctr;
      }
    }


    /*!
       \brief Apply the preconditioner.

       Calculate x = M^-1 d

       \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d)
    {
      typedef typename M::ConstRowIterator rowiteratorM;
      typedef typename M::ConstColIterator coliteratorM;
      typedef typename X::ConstIterator rowiteratorX;
      typedef typename Y::ConstIterator rowiteratorY;


      rowiteratorM endi=_A_.end();
      // std::cout << "Begin apply BlockPreconditioner" << std::endl;
      int precIDX = 0;
      rowiteratorX iv = v.begin();
      rowiteratorY id = d.begin();

      // std::cout << "prec.apply(): ";

      // Initialize copy of v_subvector that belongs to the velocity index of uzawa block 
      // to store the value when uzawa is called on the pressure block.
      XV copy_uzawa_v;
      if (hasUZAWA_){
        int v_size = v[1].N();
        copy_uzawa_v = XV(v_size, v_size);
      }

      for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      {
        int v_n = (*iv).N();
        XV vi_temp(v_n, v_n); // A zero-vector of the size of v_i
        int colctr = 0;

        for (coliteratorM j=(*i).begin(); j!=(*i).end(); ++j)
        {
          // make copy of iv
          XV copy(iv->N());
          copy *= 0.0;
          copy += *iv;
          // XV copy = iv->copy(); // only works if bvector gets a copy method
          // Do preconditioning
          if(hasUZAWA_){
            // 1: Execute UZAWA when on pressure index diagonal entry
            if(iv.index()==0 and id.index()==0){
              v_uzawa = Dumux::partial(v, Dune::index_constant<0>{}, Dune::index_constant<1>{});
              b_uzawa = Dumux::partial(d, Dune::index_constant<0>{}, Dune::index_constant<1>{});
              // CHANGE_UZAWA: Slice Matrix _A_ to have p-variables on 0-index and v variables on 1 index:
              // Do UZAWA, save value in copy_uzawa_v
              seqUzawa_.apply(v_uzawa, b_uzawa);
              vi_temp += v_uzawa[0];
              copy_uzawa_v = v_uzawa[1];
              // std::cout << precIDX << " U, ";
            }
            // 2: When on UZAWA Off Diagonals: Do Nothing
            // 3: When on UZAWA velocity index diagonal entry: Do nothing, but eventually write copy_uzawa_v to vi_temp;
            else if(iv.index()==1 and id.index()==1){
              vi_temp += copy_uzawa_v;
              // std::cout << precIDX << " -, ";
            }
            // for everything outside (00) (01) (10) and (11): apply preconditioner
            else if(iv.index()>1 or id.index()>1){
              Preconditioner<XV, YV> *_prec = _B.at(precIDX);
              _prec->apply(copy, *id);
              vi_temp += copy;
              // std::cout << precIDX << " *, ";

            }
            else{
              // std::cout << precIDX << " _, ";
            }

          // If has no uzawa, precondition everything according to given preconditioners
          }
          else if (! hasUZAWA_){
            Preconditioner<XV, YV> *_prec = _B.at(precIDX);
            _prec->apply(copy, *id);
            vi_temp += copy;
          }

          ++precIDX;
          ++id;
          ++colctr;
        }
        v[iv.index()] = vi_temp;
        ++iv;
        id -= colctr;
      }
      // std::cout << std::endl;

    }

    /*!
       \brief Clean up.

       \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x)
    {
      DUNE_UNUSED_PARAMETER(x);

      typedef typename M::ConstRowIterator rowiteratorM;
      typedef typename M::ConstColIterator coliteratorM;
      typedef typename X::ConstIterator rowiteratorX;

      rowiteratorM endi=_A_.end();
      int precIDX = 0;
      rowiteratorX ix = x.begin();

      for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      {
        int colctr = 0;
        for (coliteratorM j=(*i).begin(); j!=(*i).end(); ++j)
        {
          if (! hasUZAWA_ or (hasUZAWA_ and (ix.index()>1 or colctr>1))){
            Preconditioner<XV, YV> *_prec = _B.at(precIDX);
            _prec->post(x[ix.index()]);
          }// Uzawa.post() is empty
          precIDX += 1;
          colctr += 1;
        }
        ++ix;
      }
    }

        //! Category of the preconditioner (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }

  private:
    //! \brief the matrix we operate on.
    const M& _A_;
    //! \brief the matrix we operate on.
    const P& _B;
    const bool hasUZAWA_;
    int uzawa_it_;
    double uzawa_omega_;
    SUZW seqUzawa_ = SUZW(_A_, uzawa_it_, uzawa_omega_);
    X v_uzawa;
    Y b_uzawa;
  };





  /*!
     \Sequential MultiTypeBlockMatrix preconditioner.

     Takes a BCRS matrix of BCRS matrices and applies specified preconditioners to the submatrices.

     \tparam M The matrix type to operate on (MultiTypeBlockMatrix)
     \tparam X Type of the update (MultiTypeBlockVector)
     \tparam Y Type of the defect (MultiTypeBlockVector)
     \tparam P Tuple of tuples of PreconditionerArguments (same structure as M)
   */
  template<class M, class P, class X, class Y>
  class MTBMBlockPreconditioner : public Preconditioner<X,Y> {
  public:

    //! \brief The matrix type the preconditioner is for.
    typedef M matrix_type;
    //! \brief The domain type of the preconditioner.
    typedef X domain_type;
    //! \brief The range type of the preconditioner.
    typedef Y range_type;
    //! \brief The matrix type holding the preconditioners.
    typedef P preconditioner_type;

    //! \brief The field type of the preconditioner.
    typedef typename X::field_type field_type;


    /*! \brief Constructor.

       constructor gets all parameters to operate the prec.
       \param A The matrix to operate on.
       \param B The matrix of preconditioners to apply to the matrix.
       \param w The relaxation factor.
     */
    MTBMBlockPreconditioner (const M& A, const P& PrecArgs)
      : _A_(A), _PrecArgs(PrecArgs)
    {
      //TODO: Check if dimensions match!!!!
    }

    /*!
       \brief Prepare the preconditioner.

       \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b)
    {
      DUNE_UNUSED_PARAMETER(x);
      DUNE_UNUSED_PARAMETER(b);
      // typedef typename M::ConstRowIterator rowiteratorM;
      // typedef typename M::ConstColIterator coliteratorM;
      // typedef typename X::ConstIterator rowiteratorX;
      // typedef typename Y::ConstIterator rowiteratorY;


      // rowiteratorM endi=_A_.end();
      // int precIDX = 0;
      // rowiteratorX ix = x.begin();
      // rowiteratorY ib = b.begin();



      // for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      // {
      //   for (coliteratorM j=(*i).begin(); j!=(*i).end(); ++j)
      //   {
      //     // // auto I = Dune::index_constant<i.index()>{};
      //     // // const auto J = j.index();
      //     // constexpr I = i.index();
      //     // std::integral_constant<std::size_t, I> blub;

      //     Preconditioner<XV, YV> *_prec = _B.at(precIDX);
      //     // std::cout << ":::::::::::::::::::::::::::" << std::endl;
      //     // std::cout << "Index::::::" << j.index() << std::endl;
      //     _prec->pre(x[ix.index()], b[ib.index()]);
      //   }
      //   ++ix;
      //   ++ib;
      // }
    }

    /*!
       \brief Apply the preconditioner.

       Calculate x = M^-1 d

       \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d)
    {
      // typedef typename M::ConstRowIterator rowiteratorM;
      // typedef typename M::ConstColIterator coliteratorM;
      // typedef typename X::ConstIterator rowiteratorX;
      // typedef typename Y::ConstIterator rowiteratorY;


      // rowiteratorM endi=_A_.end();
      // // std::cout << "Begin apply BlockPreconditioner" << std::endl;
      // int precIDX = 0;
      // rowiteratorX iv = v.begin();
      // rowiteratorY id = d.begin();

      // for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      // {
        
      //   int v_n = (*iv).N();
      //   XV vi_temp(v_n, v_n); // A zero-vector of the size of v_i
      //   // std::cout << "vi_temp here:" << std::endl;
      //   // Dune::printvector(std::cout, vi_temp, "", "");
      //   // XV &copy;
      //   for (coliteratorM j=(*i).begin(); j!=(*i).end(); ++j)
      //   {
      //     XV copy = iv->copy();
      //     // std::cout << j.index() << std::endl;
      //     // Dune::printmatrix(std::cout, (*j), "", "");

      //     // Do preconditioning
      //     Preconditioner<XV, YV> *_prec = _B.at(precIDX);
      //     _prec->apply(copy, *id);

      //     vi_temp += copy;
      //     precIDX += 1;
      //   }
      //   v[iv.index()] = vi_temp;
      //   // std::cout << iv.index() << std::endl;
      //   // Dune::printvector(std::cout, (*iv), "", "");
      //   // std::cout << id.index() << std::endl;
      //   // Dune::printvector(std::cout, (*id), "", "");
      //   ++iv;
      //   ++id;
      // }
      // // std::cout << "End apply MTBMBlockPreconditioner" << std::endl;

    }

    /*!
       \brief Clean up.

       \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x)
    {
      // DUNE_UNUSED_PARAMETER(x);

      // typedef typename M::ConstRowIterator rowiteratorM;
      // typedef typename M::ConstColIterator coliteratorM;
      // typedef typename X::ConstIterator rowiteratorX;

      // rowiteratorM endi=_A_.end();
      // int precIDX = 0;
      // rowiteratorX ix = x.begin();

      // for (rowiteratorM i=_A_.begin(); i!=endi; ++i)
      // {
      //   for (coliteratorM j=(*i).begin(); j!=(*i).end(); ++j)
      //   {
      //     Preconditioner<XV, YV> *_prec = _B.at(precIDX);
      //     _prec->post(x[ix.index()]);
      //   }
      //   ++ix;
      // }
    }

        //! Category of the preconditioner (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }

  private:
    //! \brief the matrix we operate on.
    const M& _A_;
    //! \brief the preconditioner Arguments
    const P& _PrecArgs;

    // Dune::PreconditionerTuple<M, P, X, Y> PrecTup;

  };










  class PreconditionerArgs {
  public:
    //! \brief The domain type of the preconditioner.
    enum PreconditionerType {
      /**
       * @brief inverse_operator_2_preconditioner
       */
      inverse_operator_2_preconditioner,
      /**
       * @brief seqssor
       */
      seqssor,
            /**
       * @brief seqsor
       */
      seqsor,
      /**
       * @brief seqgs
       */
      seqgs,
      /**
       * @brief seqjac
       */
      seqjac,
      /**
       * @brief seqilu
       */
      seqilu,
      /**
       * @brief identity
       */
      identity,
      /**
       * @brief zero
       */
      zero,
      /**
       * @brief richardson
       */
      richardson,
      /**
       * @brief seqildl
       */
      seqildl,

      undef
    };

    PreconditionerType type = undef;


    /*! \brief Prepare the preconditioner.

       A solver solves a linear operator equation A(x)=b by applying
       one or several steps of the preconditioner. The method pre()
       is called before the first apply operation.
       b and x are right hand side and solution vector of the linear
       system respectively. It may. e.g., scale the system, allocate memory or
       compute a (I)LU decomposition.
       Note: The ILU decomposition could also be computed in the constructor
       or with a separate method of the derived method if several
       linear systems with the same matrix are to be solved.

       \param x The left hand side of the equation.
       \param b The right hand side of the equation.
     */
    virtual PreconditionerType getType () {
      return this->type;
    };

    //! every abstract base class has a virtual destructor
    virtual ~PreconditionerArgs () {}

  };

  // // This is what it should look like
  // template<class M, class X, class Y, class ArgMat, int i=0>
  // class PreconditionerTuple;

  // template<class M, class X, class Y, class ArgMat>



  // /*
  // Sample Problem:
  // Recursively constructs a tuple of size M, which corresponds to a row of Preconditioners on the input matrix
  // */
  // template<class MRow, class ArgMatRow, class XV, class YV, int i=std::tuple_size<MRow>::value-1>
  // class PreconditionerTupleRow{
  // public:
  //   // typedef decltype(std::tuple_cat(std::declval<typename PreconditionerTupleRow<M, i - 1>::type>(), std::declval<std::tuple<typename std::tuple_element< i, M>::type>>())) type;
  //   typedef decltype(std::tuple_cat(std::declval<typename PreconditionerTupleRow<MRow, ArgMatRow, XV, YV, i - 1>::type>(), std::declval<std::tuple<typename PreconditionerType<std::get<i>(MRow), XV, YV, std::get<i>(ArgMatRow)>::type>>())) type;
  // };

  // template<class MRow, class ArgMatRow, class XV, class YV>
  // class PreconditionerTupleRow<MRow, ArgMatRow, XV, YV, 0>{
  // public:
  //   typedef std::tuple<typename PreconditionerType<std::get<0>(MRow), XV, YV, std::get<0>(ArgMatRow)>::type> type;

  // };



  // template<class M, class ArgMat, class X, class Y, int i=std::tuple_size<M>::value-1>
  // class PreconditionerTuple{
  // public:
  //   // typedef decltype(std::tuple_cat(std::declval<typename PreconditionerTuple<M, i - 1>::type>(), std::declval<std::tuple<typename std::tuple_element< i, M>::type>>())) type;
  //   typedef decltype(std::tuple_cat(std::declval<typename PreconditionerTuple<M, ArgMat, X, Y, i - 1>::type>(), std::declval<std::tuple<typename PreconditionerTupleRow<std::get<i>(M), std::get<i>(ArgMat), std::get<i>(X), std::get<i>(Y)>::type>>())) type;
  // };

  // template<class M, class ArgMat, class X, class Y>
  // class PreconditionerTuple<M, ArgMat, X, Y, 0>{
  // public:
  //   typedef std::tuple<typename PreconditionerTupleRow<std::get<0>(M), std::get<0>(ArgMat), std::get<0>(X), std::get<0>(Y)>::type> type;

  // };
  




  // template< class M, class X, class Y, class ArgType>
  // class PreconditionerType;

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, ArgsSeqSSOR>{
  //   typedef Dune::SeqSSOR<M, X, Y> type;
  // };

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, Dune::ArgsSeqSOR>{
  //   typedef Dune::SeqSOR<M, X, Y> type;
  // };

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, Dune::ArgsSeqGS>{
  //   typedef Dune::SeqGS<M, X, Y> type;
  // };

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, Dune::ArgsSeqJac>{
  //   typedef Dune::SeqJac<M, X, Y> type;
  // };

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, Dune::ArgsSeqILU>{
  //   typedef Dune::SeqILU<M, X, Y> type;
  // };

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, Dune::ArgsIdentity>{
  //   typedef Dune::Identity<X, Y> type;
  // };

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, Dune::ArgsZero>{
  //   typedef Dune::Zero<X, Y> type;
  // };

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, Dune::ArgsRichardson>{
  //   typedef Dune::Richardson<X, Y> type;
  // };

  // template< class M, class X, class Y>
  // class PreconditionerType<M, X, Y, Dune::ArgsSeqILDL>{
  //   typedef Dune::SeqILDL<M, X, Y> type;
  // };

  // Do rest






  
  // template< size_t n, size_t m, class M, class X, class Y, class PA> // m,n not required?
  // class PreconditionerMatrix
  // {
  // public:

  //   //! \brief The matrix type the preconditioner is for.
  //   typedef M matrix_type;
  //   //! \brief The domain type of the preconditioner.
  //   typedef X domain_type;
  //   //! \brief The range type of the preconditioner.
  //   typedef Y range_type;
  //       //! \brief The matrix type of the matrix holding the PreconditionerArguments.
  //   typedef PA precargsmat_type;


  //   PreconditionerMatrix (const M& M, const X& x, const Y& rhs, const PA& precargs)
  //     : M_(M), x_(x), rhs_(rhs), precargs_(precargs)
  //   {
  //     //TODO: Check if dimensions match!!!!
  //   }


  //   template<int k>
  //   std::vector<Simplex<k>> & getElement() // correct return type
  //   {
  //       return std::get<k>(row_); // Does this work for tuple?
  //   }
 
  // private:
  //   // Simplex -> Preconditioner<decltype(x_m), decltype(rhs_m)>
  //   template<size_t I, size_t... ks >
  //   auto x_i = x_[I];
  //   auto rhs_i = rhs_[I];
  //   using RowTuple = std::tuple<Preconditioner<decltype(x_i), decltype(rhs_i)>...>; // ks fehlt!!!
 
  //   template<size_t I, size_t... ks>
  //   static RowTuple<I, (ks)...> make_row(I, std::index_sequence<ks...>)
  //   {
  //       return RowTuple<I, (ks)...>{};
  //   }
 
  //   template<size_t I, size_t J> // I = #row)
  //   static auto make_row()
  //   {
  //       return make_row(I, std::make_index_sequence<J>{});
  //   }
 
  //   size_t i = 0;
  //   using Row = decltype(make_row<i, M.M()>());
 
  //   Row row_;
  //   M& M_;
  //   X& x_; 
  //   Y& rhs_; 
  //   PA& precargs_;


 
  // };

/**
 * @}
 */
}
#endif
