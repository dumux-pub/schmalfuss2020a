// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_ISTL_PRECONDITIONERS_HH_XT
#define DUNE_ISTL_PRECONDITIONERS_HH_XT

#include <dune/istl/preconditioners.hh>
#include "preconditioner_xt.hh"

#include <cmath>
#include <complex>
#include <iostream>
#include <iomanip>
#include <memory>
#include <string>

#include <dune/common/simd/simd.hh>
#include <dune/common/unused.hh>
// Added by Jenny
#include <dune/common/hybridutilities.hh>
// #include <dune/istl/bcrsmatrix.hh>
// #include "multitypeblockmatrix.hh"



#include <config.h>
#include <ctime>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
//#include <dune/istl/io.hh>
// End Added by Jenny



namespace Dune {
  /** @defgroup ISTL_Prec Preconditioners
   * @ingroup ISTL_Solvers
   *
   * All of our \ref ISTL_Solvers "Krylow solvers" are preconditioned versions.
   * There are sequential preconditioners (e,g. SeqJacobi, SeqSOR, SeqSSOR) as well as parallel preconditioners
   * (e.g. AMG, BlockPreconditioner) available for plugging them into the solvers
   * together with matching ScalarProducts.
   *
   * Some of the available preconditioners (e.g. SeqJacobi, SeqSOR, SeqSSOR))
   * may be given an aditional int as a template parameter, the block recursion level.
   * These preconditioners
   * can be used on block-recursive matrices with an arbitrary hierarchy depth
   * (eg. BCRSMatrix<BCRSMatrix<FieldMatrix,n,m> > >. Given a block recursion level
   * \f$k\f$ those preconditioners work as
   * normal on the offdiagonal blocks, treating them as traditional matrix
   * entries. For the diagonal values a special procedure applies:  If
   * \f$k>1\f$ the diagonal is treated as a matrix itself and the preconditioner
   * is applied recursively on the matrix representing the diagonal value
   * \f$D=A_{ii}\f$ with block level \f$k-1\f$. For the case that \f$k=1\f$ the diagonal
   * is treated as a
   * matrix entry resulting in a linear solve or an identity operation
   * depending on the algorithm.
   */

  /** @addtogroup ISTL_Prec
          @{
   */
  /** \file

     \brief    Define general preconditioner interface

     Wrap the methods implemented by ISTL in this interface.
     However, the interface is extensible such that new preconditioners
     can be implemented and used with the solvers.
   */



  /*!
     \brief Argument Object for InverseOperator2Preconditioner Preconditioner
   */
  template<class O>
  class ArgsInverseOperator2Preconditioner : public PreconditionerArgs {

  public:
    typedef O InverseOperator;

    ArgsInverseOperator2Preconditioner (InverseOperator& inverse_operator, int c = -1)
    {
      this->type = inverse_operator_2_preconditioner;
      this->c = c;
      this->inverse_operator = inverse_operator;
    }

    int get_c(){
      return this->c;
    }

    double get_inverseOperator(){
      return this->inverse_operator;
    }

  private:
    int c = -1;
    InverseOperator& inverse_operator;
  };




        /*!
     \brief Argument Object for SeqSSOR Preconditioner
   */
  class ArgsSeqSSOR : public PreconditionerArgs {

  public:

    ArgsSeqSSOR (int n, double w)
    {
      this->type = seqssor;
      this->n = n;
      this->w = w;
    }

    int get_n(){
      return this->n;
    }

    double get_w(){
      return this->w;
    }

  private:
    double w;
    int n;
  };



        /*!
     \brief Argument Object for SeqSOR Preconditioner
   */
  class ArgsSeqSOR : public PreconditionerArgs {

  public:

    ArgsSeqSOR (int n, double w)
    {
      this->type = seqsor;
      this->n = n;
      this->w = w;
    }

    int get_n(){
      return this->n;
    }

    double get_w(){
      return this->w;
    }

  private:
    double w;
    int n;
  };



        /*!
     \brief Argument Object forSeqGS Preconditioner
   */
  class ArgSeqGS : public PreconditionerArgs {

  public:

    ArgSeqGS (int n, double w)
    {
      this->type = seqgs;
      this->n = n;
      this->w = w;
    }

    int get_n(){
      return this->n;
    }

    double get_w(){
      return this->w;
    }

  private:
    double w;
    int n;
  };

      /*!
     \brief Argument Object for SeqJac Preconditioner
   */
  class ArgsSeqJac : public PreconditionerArgs {

  public:

    ArgsSeqJac (int n, double w)
    {
      this->type = seqjac;
      this->n = n;
      this->w = w;
    }

    int get_n(){
      return this->n;
    }

    double get_w(){
      return this->w;
    }

  private:
    double w;
    int n;
  };


    /*!
     \brief Argument Object for SeqILU Preconditioner
   */
  class ArgsSeqILU : public PreconditionerArgs {

  public:
    ArgsSeqILU (double w, const bool resort = false )
    {
      this->type = seqilu;
      this->w = w;
      this->resort = resort;
    }

    ArgsSeqILU (int n, double w, const bool resort = false )
    {
      this->type = seqilu;
      this->n = n;
      this->w = w;
      this->resort = resort;
    }

    int get_n(){
      return this->n;
    }

    double get_w(){
      return this->w;
    }

    bool get_resort(){
      return this->resort;
    }

  private:
    double w;
    int n = 0;
    bool resort = false;
  };



    /*!
     \brief Identity preconditioner.

     A preconditioner that does nothing.

     \tparam X Type of the update
     \tparam Y Type of the defect
   */
  template<class X, class Y>
  class Identity : public Preconditioner<X,Y> {
  public:
    //! \brief The domain type of the preconditioner.
    typedef X domain_type;
    //! \brief The range type of the preconditioner.
    typedef Y range_type;

    //! \brief The field type of the preconditioner.
    typedef typename X::field_type field_type;

   /*! \brief Constructor.

       Constructor invoking Identity preconditioner
       
     */
    Identity ()
      {}

    Identity (double w){
      this->w = w;
    }

    /*!
       \brief Prepare the preconditioner.

       \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b)
    {
      DUNE_UNUSED_PARAMETER(x);
      DUNE_UNUSED_PARAMETER(b);
    }

    /*!
       \brief Apply the preconditoner.

       \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d)
    {
      identmult(v,d,this->w);
    }

    /*!
       \brief Clean up.

       \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x)
    {
      DUNE_UNUSED_PARAMETER(x);
    }

    //! Category of the preconditioner (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }

  private:
    void identmult (X& x, const Y& b, double w=1.0)
    {
      typedef typename Y::ConstIterator rowIteratorB;
      typedef typename X::ConstIterator rowIteratorX;
      
      rowIteratorB endi=b.end();
      rowIteratorX xi = x.begin();
      for (rowIteratorB i=b.begin(); i!=endi; ++i)
      {
        x[xi.index()] = (*i)*w;
        ++xi;
      }
    }

    double w = 1.0;

  };



  /*!
     \brief Preconditioner for upper off-diagonal stokes block.
      
     \tparam M Matrix type to operate on
     \tparam X Type of the update
     \tparam Y Type of the defect
     \tparam PS: Type of precondioner for non-zero-diagonal stokes block
     \tparam PZ: Type of precondioner for zero-diagonal stokes block (usually Identity)
   */
  template<class M, class X, class Y, class PS, class PZ>
  class StokesOffDiagUpper : public Preconditioner<X,Y> {
  public:

        //! \brief The matrix type the preconditioner is for.
    typedef typename std::remove_const<M>::type matrix_type;
    //! block type of matrix
    typedef typename matrix_type :: block_type block_type;
    //! \brief The domain type of the preconditioner.
    typedef X domain_type;
    //! \brief The range type of the preconditioner.
    typedef Y range_type;

    //! \brief The field type of the preconditioner.
    typedef typename X::field_type field_type;

    //! \brief scalar type underlying the field_type
    typedef Simd::Scalar<field_type> scalar_field_type;

    //! \brief type of ILU storage
    typedef typename ILU::CRS< block_type > CRS;

   /*! \brief Constructor.

       Constructor invoking Identity preconditioner
       
     */
    StokesOffDiagUpper (const M& A, PS& diagprecond, PZ& identprecond )
      : block_{A},
        p_nonzero_(diagprecond),
        p_zero_(identprecond)
    {}

    /*!
       \brief Prepare the preconditioner.

       \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b)
    {
      p_nonzero_.pre(x,b);
      p_zero_.pre(x,b);
    }

    /*!
       \brief Apply the preconditoner.

       \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d)
    {
      // std::cout << "Apply StokesOffDiagUpper to defect of size " << d.N() << " and x " << v.N() << std::endl;
      int d_n = d.N();
      X v_temp(d_n, d_n);

      p_nonzero_.apply(v_temp,d);
      // std::cout << "Apply StokesOffDiagUpper multiply " << block_.N() << "x" << block_.M() << " with " << v_temp.N() << std::endl;

      block_.mv(v_temp,v);
      p_zero_.apply(v,v);
      v*=-1.0;
    }

    /*!
       \brief Clean up.

       \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x)
    {
      p_nonzero_.post(x);
      p_zero_.post(x);
    }

  private:
    const M& block_;
    PS& p_nonzero_;
    PZ& p_zero_;

  };



    /*!
     \brief Preconditioner for lower off-diagonal stokes block.
      
     \tparam M Matrix type to operate on
     \tparam X Type of the update
     \tparam Y Type of the defect
     \tparam PS: Type of precondioner for non-zero-diagonal stokes block
     \tparam PZ: Type of precondioner for zero-diagonal stokes block (usually Identity)
   */
  template<class M, class X, class Y, class PS, class PZ>
  class StokesOffDiagLower : public Preconditioner<X,Y> {
  public:

        //! \brief The matrix type the preconditioner is for.
    typedef typename std::remove_const<M>::type matrix_type;
    //! block type of matrix
    typedef typename matrix_type :: block_type block_type;
    //! \brief The domain type of the preconditioner.
    typedef X domain_type;
    //! \brief The range type of the preconditioner.
    typedef Y range_type;

    //! \brief The field type of the preconditioner.
    typedef typename X::field_type field_type;

    //! \brief scalar type underlying the field_type
    typedef Simd::Scalar<field_type> scalar_field_type;

    //! \brief type of ILU storage
    typedef typename ILU::CRS< block_type > CRS;

   /*! \brief Constructor.

       Constructor invoking Identity preconditioner
       
     */
    StokesOffDiagLower (const M& A, PS& lower, PZ& upper )
      : block_{A},
        p_lower_(lower),
        p_upper_(upper)
    {
      int d_upper = block_.M();
      int d_lower = block_.N();
      X x_d(d_upper, d_upper);
      Y d_d(d_lower, d_lower);
      x_upper_ = x_d;
      d_lower_ = d_d;
    }

    /*!
       \brief Prepare the preconditioner.

       \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b)
    {
      // std::cout << "ST_OFF_LOWER_PRE: NxM, x.N(), b.N() : " << block_.N() << "x" << block_.M() << " " << x.N() << " " << b.N() << std::endl;
      // DO NOHTING: x and b are already modified by preconditioners on the diagonal, don't do anything here to prevent double processing.
      // p_upper_.pre(x_upper_,b);
      // p_lower_.pre(x,d_lower_);
    }

    /*!
       \brief Apply the preconditoner.

       \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& x, const Y& d)
    {
      // std::cout << "Apply StokesOffDiagLower to defect of size " << d.N() << " and x " << x.N() << std::endl;
      // std::cout << "Apply StokesOffDiagLower multiply " << block_.N() << "x" << block_.M() << " with " << x_lower_.N() << std::endl;
      // std::cout << "STOKES_OFF_LOWER_APPLY: A*x = d, A.N()xA.M(), x.N(), d.N()" << block_.N() << "x" << block_.M() << " " << x_upper_.N() << " " << d_lower_.N() << std::endl;

      // Important: Explicitly set x_upper_ and x to zero before applying the preconditioner to obtain linear solvers in case of Jacobi (and AMG)
      x_upper_ *= 0.0;
      p_upper_.apply(x_upper_,d);
      block_.mv(x_upper_,d_lower_);
      x *= 0.0;
      p_lower_.apply(x,d_lower_);
      x *= -1.0;
    }

    /*!
       \brief Clean up.

       \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x)
    {
      // DO NOTHING: All preconditioners on diagonal of blocky preconditioner will be deleted when blocky reaches the diagonal.
      // To prevent double frees, don't do anything here.
      // p_upper_.post(x_upper_);
      // p_lower_.post(x);
    }

  private:
    const M& block_;
    PS& p_lower_;
    PZ& p_upper_;
    X x_upper_;
    Y d_lower_;

  };




  // /*!
  //    \brief Argument Object for Identity Preconditioner
  //  */
  // class StokesOffDiag : public PreconditionerArgs {

  // public:

  //   StokesOffDiag ()
  //   {
  //     this->type = identity;
  //   }

  //   StokesOffDiag (double w)
  //   {
  //     this->type = identity;
  //     this->w = w;
  //   }

  //   double get_w(){
  //     return this->w;
  //   }

  // private:
  //   double w = 1.0;
  // };



     /*!
     \brief Zero preconditioner.

     A preconditioner that multiplies every entry with 0

     \tparam X Type of the update
     \tparam Y Type of the defect
   */
  template<class X, class Y>
  class Zero : public Preconditioner<X,Y> {
  public:
    //! \brief The domain type of the preconditioner.
    typedef X domain_type;
    //! \brief The range type of the preconditioner.
    typedef Y range_type;

    //! \brief The field type of the preconditioner.
    typedef typename X::field_type field_type;

   /*! \brief Constructor.

       Constructor invoking Zero preconditioner
       \param A The matrix to operate on.
       \param n The number of iterations to perform.
       \param w The relaxation factor.
       \param resort true if a resort of the computed ILU for improved performance should be done.
     */
    Zero ()
      {}

    /*!
       \brief Prepare the preconditioner.

       \copydoc Preconditioner::pre(X&,Y&)
     */
    virtual void pre (X& x, Y& b)
    {
      DUNE_UNUSED_PARAMETER(x);
      DUNE_UNUSED_PARAMETER(b);
    }

    /*!
       \brief Apply the preconditoner.

       \copydoc Preconditioner::apply(X&,const Y&)
     */
    virtual void apply (X& v, const Y& d)
    {
      v *= 0.0;
    }

    /*!
       \brief Clean up.

       \copydoc Preconditioner::post(X&)
     */
    virtual void post (X& x)
    {
      DUNE_UNUSED_PARAMETER(x);
    }

    //! Category of the preconditioner (see SolverCategory::Category)
    virtual SolverCategory::Category category() const
    {
      return SolverCategory::sequential;
    }
    
  };


    /*!
     \brief Argument Object for Zero Preconditioner
   */
  class ArgsZero : public PreconditionerArgs {

  public:

    ArgsZero ()
    {
      this->type = zero;
    }

  };





    /*!
     \brief Argument Object for SeqILDL Preconditioner
   */
  class ArgsSeqILDL : public PreconditionerArgs {

  public:

    ArgsSeqILDL (double relax)
    {
      this->type = seqildl;
      this->relax = relax;
    }

    double get_relax(){
      return this->relax;
    }

  private:
    double relax = 1.0;
  };

  /** @} end documentation */

} // end namespace

#endif
