// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_ISTL_PRECONDITIONERMATRIX_HH
#define DUNE_ISTL_PRECONDITIONERMATRIX_HH

#include <cmath>
#include <complex>
#include <iostream>
#include <iomanip>
#include <memory>
#include <string>

#include <dune/common/simd/simd.hh>
#include <dune/common/unused.hh>
// Added by Jenny
#include <dune/common/hybridutilities.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/multitypeblockmatrix.hh>

#include <config.h>
#include <ctime>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>
#include <dune/istl/preconditioner_xt.hh>
#include <dune/istl/preconditioners_xt.hh>
// End Added by Jenny

#include <dune/istl/preconditioner.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solver.hh>
#include <dune/istl/solvercategory.hh>
#include <dune/istl/istlexception.hh>
#include <dune/istl/matrixutils.hh>
#include <dune/istl/gsetc.hh>
#include <dune/istl/ildl.hh>
#include <dune/istl/ilu.hh>


namespace Dune {

	/*
	Define get functions to use on MultiTypeBlockVector
	*/
  template<size_t I, typename ...Args>
	constexpr auto get(Dune::MultiTypeBlockVector <Args...>& vec) { return vec[I]; }

	template<size_t I, typename FirstRow, typename ...Args>
	constexpr auto get(Dune::MultiTypeBlockMatrix <FirstRow, Args...>& vec) { return vec[I]; }



/////////////////////////////////////////////////////////////////////////////////////

  /*
	Make MultiTypeBlockVector usable with std::tuple_size for better integration into StoragePreconditionerMatrix / StoragePreconditionerRow
  */
	namespace std {
	  template<typename... Args>
	  class tuple_size<Dune::MultiTypeBlockVector <Args...> > : public std::integral_constant<size_t, sizeof...(Args)> {};

	  template<size_t I, typename... Args>
		class tuple_element< I, Dune::MultiTypeBlockVector <Args...>  > { public: using type = typename std::tuple_element<I,std::tuple<Args...>>::type; };


	  template<typename FirstRow, typename... Args>
	  class tuple_size<Dune::MultiTypeBlockMatrix <FirstRow, Args...> > : public std::integral_constant<size_t, sizeof...(Args) + 1> {};

	  template<size_t I, typename FirstRow, typename... Args>
		class tuple_element< I, Dune::MultiTypeBlockMatrix <FirstRow, Args...>  > { public: using type = typename std::tuple_element<I,std::tuple<FirstRow, Args...>>::type; };
	}


	/////////////////////////////////////////////////////////////////////////////////////




	template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	class TupleAssignment{
	public:
	  static void assign(T& t, Z& z){
	  	std::get<i>(t) = std::get<i>(z);
	  	TupleAssignment<T, Z, i-1>::assign(t, z);
	  }
	};

  template<class T, class Z>
	class TupleAssignment<T, Z, 0u>{
	public:
	  static void assign(T& t, Z& z){
	  	std::get<0>(t) = std::get<0>(z);
	  }
	};


	template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	class TupleMatrixAssignment{
	public:
	  static void assign(T& t, Z& z){
	  	TupleAssignment<typename std::tuple_element< i, T>::type,typename std::tuple_element< i, Z>::type>::assign(std::get<i>(t), std::get<i>(z));
	  	TupleMatrixAssignment<T, Z, i-1>::assign(t, z);
	  }
	};

  template<class T, class Z>
	class TupleMatrixAssignment<T, Z, 0u>{
	public:
	  static void assign(T& t, Z& z){
	  	TupleAssignment<typename std::tuple_element< 0, T>::type,typename std::tuple_element< 0, Z>::type>::assign(std::get<0>(t), std::get<0>(z));
	  }
	};


	
	template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	void assignTuple(T& t, Z& z){
  	TupleAssignment<T, Z, i>::assign(t, z);
  }

  template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	void assignTupleMatrix(T& t, Z& z){
  	TupleMatrixAssignment<T, Z, i>::assign(t, z);
  }



/////////////////////////////////////////////////////////////////////////////////////



  template<class T, int i=std::tuple_size<T>::value-1>
  class StoragePreconditionerRow{
  public:
  	typedef int currElem;
  	typedef typename StoragePreconditionerRow<T, i - 1>::type prevElems;
  	typedef decltype(std::tuple_cat(std::declval<prevElems>(), std::declval<std::tuple<currElem>>())) type;

  	static const int size = i;
  };

  template<class T>
  class StoragePreconditionerRow<T, 0>{
  public:
  	typedef int currElem; 
  	typedef std::tuple<currElem> type;

  	static const int size = 0;
  };


  template<class MT, int j=std::tuple_size<MT>::value-1>
  class StoragePreconditionerMatrix{
  public:
  	// typedef decltype(std::tuple_cat(std::declval<typename StoragePreconditionerMatrix<MT, i - 1>::type>(), std::declval<std::tuple<typename StoragePreconditionerRow<std::get<i>(MT)>::type>>())) type;
  	typedef typename StoragePreconditionerRow<typename std::tuple_element< j, MT>::type>::type currElem;
  	typedef decltype(std::tuple_cat(std::declval<typename StoragePreconditionerMatrix<MT, j - 1>::type>(), std::declval<std::tuple<currElem>>())) type;
  };

  template<class MT>
  class StoragePreconditionerMatrix<MT, 0>{
  public:
  	// typedef std::tuple<typename StoragePreconditionerRow<std::get<0>(MT)>::type> type;
  	typedef typename StoragePreconditionerRow<typename std::tuple_element< 0, MT>::type>::type currElem;
  	typedef std::tuple<currElem> type;
  };

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

	// namespace Dune {
	  template<class M, class X, class Y, class PA>
	  class PreconditionerMatrix{
	  public:
	  	typedef typename StoragePreconditionerMatrix<M>::type type;

	  	PreconditionerMatrix (const M& mat, X& x, Y& d, PA& precArgsMat)
      : mat_(mat), x_(x), d_(d), precArgsMat_(precArgsMat) {
      	assignTupleMatrix(precMat_, precArgsMat_);
      }

      static constexpr std::size_t N()
		  {
		    return std::tuple_size<PA>::value;
		  }

		  /** \brief Return the number of matrix rows */
		  static constexpr std::size_t size()
		  {
		    return std::tuple_size<PA>::value;
		  }

		  auto getPreconditionerMatrix(){
		  	return this->precMat_;
		  }

		  // /** \brief Return the number of matrix columns */
		  // static constexpr std::size_t M()
		  // {
		  //   return std::tuple_size<typename std::tuple_element< 0, M>::type>::value;
		  // }

		  template< std::size_t row, std::size_t column >
	    auto get ( const std::integral_constant< std::size_t, row > rowVariable, const std::integral_constant< std::size_t, column > columnVariable )
	    {
	      DUNE_UNUSED_PARAMETER(rowVariable);
	      DUNE_UNUSED_PARAMETER(columnVariable);
	      return std::get<column>(std::get<row>(precMat_));
	    }

	  private:
	  	const M& mat_;
	  	X& x_;
	  	Y& d_;
	  	PA& precArgsMat_;
	  	// this::type precMat_;
	  	typename StoragePreconditionerMatrix<M>::type precMat_;

	  };




} // end namespace

#endif