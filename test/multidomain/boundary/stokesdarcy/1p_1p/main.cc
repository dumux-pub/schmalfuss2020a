// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup BoundaryTests
 * \brief A test problem for the coupled Stokes/Darcy problem (1p).
 */

#include <config.h>

#include <ctime>
#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/istl/io.hh>
#include <dune/istl/matrixmarket.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/partial.hh>
#include <dumux/common/partial_xt.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/seqsolverbackend_xt.hh>
#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/io/vtkoutputmodule.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/io/grid/gridmanager.hh>

#include <dumux/multidomain/staggeredtraits.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/newtonsolver.hh>

#include <dumux/multidomain/boundary/stokesdarcy/couplingmanager.hh>

#include <dumux/linear/matrixconverter_xt.hh>
#include "problem_darcy.hh"
#include "problem_stokes.hh"

namespace Dumux {
namespace Properties {

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::StokesOneP>
{
    using Traits = StaggeredMultiDomainTraits<TypeTag, TypeTag, Properties::TTag::DarcyOneP>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

template<class TypeTag>
struct CouplingManager<TypeTag, TTag::DarcyOneP>
{
    using Traits = StaggeredMultiDomainTraits<Properties::TTag::StokesOneP, Properties::TTag::StokesOneP, TypeTag>;
    using type = Dumux::StokesDarcyCouplingManager<Traits>;
};

} // end namespace Properties
} // end namespace Dumux

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // Define the sub problem type tags
    using StokesTypeTag = Properties::TTag::StokesOneP;
    using DarcyTypeTag = Properties::TTag::DarcyOneP;

    // try to create a grid (from the given grid file or the input file)
    // for both sub-domains
    using DarcyGridManager = Dumux::GridManager<GetPropType<DarcyTypeTag, Properties::Grid>>;
    DarcyGridManager darcyGridManager;
    darcyGridManager.init("Darcy"); // pass parameter group

    using StokesGridManager = Dumux::GridManager<GetPropType<StokesTypeTag, Properties::Grid>>;
    StokesGridManager stokesGridManager;
    stokesGridManager.init("Stokes"); // pass parameter group

    // we compute on the leaf grid view
    const auto& darcyGridView = darcyGridManager.grid().leafGridView();
    const auto& stokesGridView = stokesGridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using StokesFVGridGeometry = GetPropType<StokesTypeTag, Properties::FVGridGeometry>;
    auto stokesFvGridGeometry = std::make_shared<StokesFVGridGeometry>(stokesGridView);
    stokesFvGridGeometry->update();
    using DarcyFVGridGeometry = GetPropType<DarcyTypeTag, Properties::FVGridGeometry>;
    auto darcyFvGridGeometry = std::make_shared<DarcyFVGridGeometry>(darcyGridView);
    darcyFvGridGeometry->update();

    using Traits = StaggeredMultiDomainTraits<StokesTypeTag, StokesTypeTag, DarcyTypeTag>;

    // the coupling manager
    using CouplingManager = StokesDarcyCouplingManager<Traits>;
    auto couplingManager = std::make_shared<CouplingManager>(stokesFvGridGeometry, darcyFvGridGeometry);

    // the indices
    constexpr auto stokesCellCenterIdx = CouplingManager::stokesCellCenterIdx;
    constexpr auto stokesFaceIdx = CouplingManager::stokesFaceIdx;
    constexpr auto darcyIdx = CouplingManager::darcyIdx;

    // the problem (initial and boundary conditions)
    using StokesProblem = GetPropType<StokesTypeTag, Properties::Problem>;
    auto stokesProblem = std::make_shared<StokesProblem>(stokesFvGridGeometry, couplingManager);
    using DarcyProblem = GetPropType<DarcyTypeTag, Properties::Problem>;
    auto darcyProblem = std::make_shared<DarcyProblem>(darcyFvGridGeometry, couplingManager);

    // the solution vector
    Traits::SolutionVector sol;
    sol[stokesCellCenterIdx].resize(stokesFvGridGeometry->numCellCenterDofs());
    sol[stokesFaceIdx].resize(stokesFvGridGeometry->numFaceDofs());
    sol[darcyIdx].resize(darcyFvGridGeometry->numDofs());

    // get a solution vector storing references to the two Stokes solution vectors
    auto stokesSol = partial(sol, stokesCellCenterIdx, stokesFaceIdx);

    couplingManager->init(stokesProblem, darcyProblem, sol);

    // the grid variables
    using StokesGridVariables = GetPropType<StokesTypeTag, Properties::GridVariables>;
    auto stokesGridVariables = std::make_shared<StokesGridVariables>(stokesProblem, stokesFvGridGeometry);
    stokesGridVariables->init(stokesSol);
    using DarcyGridVariables = GetPropType<DarcyTypeTag, Properties::GridVariables>;
    auto darcyGridVariables = std::make_shared<DarcyGridVariables>(darcyProblem, darcyFvGridGeometry);
    darcyGridVariables->init(sol[darcyIdx]);

    // intialize the vtk output module
    StaggeredVtkOutputModule<StokesGridVariables, decltype(stokesSol)> stokesVtkWriter(*stokesGridVariables, stokesSol, stokesProblem->name());
    GetPropType<StokesTypeTag, Properties::IOFields>::initOutputModule(stokesVtkWriter);
    stokesVtkWriter.write(0.0);

    VtkOutputModule<DarcyGridVariables, GetPropType<DarcyTypeTag, Properties::SolutionVector>> darcyVtkWriter(*darcyGridVariables, sol[darcyIdx],  darcyProblem->name());
    using DarcyVelocityOutput = GetPropType<DarcyTypeTag, Properties::VelocityOutput>;
    darcyVtkWriter.addVelocityOutput(std::make_shared<DarcyVelocityOutput>(*darcyGridVariables));
    GetPropType<DarcyTypeTag, Properties::IOFields>::initOutputModule(darcyVtkWriter);
    darcyVtkWriter.write(0.0);

    // the assembler for a stationary problem
    using Assembler = MultiDomainFVAssembler<Traits, CouplingManager, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(std::make_tuple(stokesProblem, stokesProblem, darcyProblem),
                                                 std::make_tuple(stokesFvGridGeometry->cellCenterFVGridGeometryPtr(),
                                                                 stokesFvGridGeometry->faceFVGridGeometryPtr(),
                                                                 darcyFvGridGeometry),
                                                 std::make_tuple(stokesGridVariables->cellCenterGridVariablesPtr(),
                                                                 stokesGridVariables->faceGridVariablesPtr(),
                                                                 darcyGridVariables),
                                                 couplingManager);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    NewtonSolver nonLinearSolver(assembler, linearSolver, couplingManager);

    bool writetime = getParam<bool>("UsedSolver.WriteTime", false);
    std::string timefilename = getParam<std::string>("UsedSolver.TimeFile", "TimeFile.txt");


    bool useUmfpack = getParam<bool>("UsedSolver.Umfpack", false);
    if (useUmfpack){
        Dune::Timer watch(false);
        watch.start();
        // solve the non-linear system
        nonLinearSolver.solve(sol);

        double solvertime = watch.elapsed();
        std::cout << "TIME incl setup: " << solvertime << "s" << std::endl;

        if (writetime){
            std::ofstream resfile;
            resfile.open(timefilename, std::ios::out );
            resfile << "TIME excl setup: " << 0.0 << " s\n";
            resfile << "TIME incl setup: " << solvertime << " s\n";
            resfile.close();
        }
    }
    else{


        ///////////////////////// Begin Jenny


        nonLinearSolver.assembleLinearSystem(sol);

        const auto& M = assembler->jacobian();
        auto& rhs = assembler->residual();
        std::cout << M.N() << std::endl;

        using Vector = Dune::BlockVector<Dune::FieldVector<double, 1>>;
        using Matrix = Dune::BCRSMatrix<Dune::FieldMatrix<double, 1>>;
        using VecCont = Dune::BlockVector<Vector>;
        using MatCont = Dune::BCRSMatrix<Matrix>;

        const MatCont  A_ = MatrixConverterExt<decltype(M), double>::multiTypeToBcrsBcrsMatrix(M);
        const VecCont  b_ = VectorConverterExt<decltype(rhs), double>::multiTypeToBlockBlockVector(rhs);
        VecCont xx_ = VectorConverterExt<decltype(sol), double>::multiTypeToBlockBlockVector(sol);

        bool outputmatrix(false);

        if (outputmatrix){
            std::string fnme00 = "solvermat00.mat";
            Dune::writeMatrixToMatlab(A_[0][0], fnme00);
            std::string fnme01 = "solvermat01.mat";
            Dune::writeMatrixToMatlab(A_[0][1], fnme01);
            std::string fnme02 = "solvermat02.mat";
            Dune::writeMatrixToMatlab(A_[0][2], fnme02);
            std::string fnme10 = "solvermat10.mat";
            Dune::writeMatrixToMatlab(A_[1][0], fnme10);
            std::string fnme11 = "solvermat11.mat";
            Dune::writeMatrixToMatlab(A_[1][1], fnme11);
            std::string fnme12 = "solvermat12.mat";
            Dune::writeMatrixToMatlab(A_[1][2], fnme12);
            std::string fnme20 = "solvermat20.mat";
            Dune::writeMatrixToMatlab(A_[2][0], fnme20);
            std::string fnme21 = "solvermat21.mat";
            Dune::writeMatrixToMatlab(A_[2][1], fnme21);
            std::string fnme22 = "solvermat22.mat";
            Dune::writeMatrixToMatlab(A_[2][2], fnme22);
            std::cout << "WROTE MATRIX" << std::endl;

            std::string fnmex0 = "solverx0.mat";
            Dune::writeVectorToMatlab(xx_[0], fnmex0);
            std::string fnmex1 = "solverx1.mat";
            Dune::writeVectorToMatlab(xx_[1], fnmex1);
            std::string fnmex2 = "solverx2.mat";
            Dune::writeVectorToMatlab(xx_[2], fnmex2);
            std::cout << "WROTE VEC X" << std::endl;

            std::string fnmeb0 = "solverb0.mat";
            Dune::writeVectorToMatlab(b_[0], fnmeb0);
            std::string fnmeb1 = "solverb1.mat";
            Dune::writeVectorToMatlab(b_[1], fnmeb1);
            std::string fnmeb2 = "solverb2.mat";
            Dune::writeVectorToMatlab(b_[2], fnmeb2);
            std::cout << "WROTE VEC B" << std::endl;
        }

        std::cout << "Matrix dimensions ln 1: " << A_[0][0].N() << "x" << A_[0][0].M() << "   " << A_[0][1].N() << "x" << A_[0][1].M() << "   " << A_[0][2].N() << "x" << A_[0][2].M() << std::endl;
        std::cout << "Matrix dimensions ln 2: " << A_[1][0].N() << "x" << A_[1][0].M() << "   " << A_[1][1].N() << "x" << A_[1][1].M() << "   " << A_[1][2].N() << "x" << A_[1][2].M() << std::endl;
        std::cout << "Matrix dimensions ln 3: " << A_[2][0].N() << "x" << A_[2][0].M() << "   " << A_[2][1].N() << "x" << A_[2][1].M() << "   " << A_[2][2].N() << "x" << A_[2][2].M() << std::endl;
        // Dune::printmatrix(std::cout, A[0][1], "", "");
        // Dune::printmatrix(std::cout, A[1][0], "", "");
        // Dune::printmatrix(std::cout, A[1][1], "", "");
        // Dune::printmatrix(std::cout, A[0][2], "", "");
        // Dune::printmatrix(std::cout, A[2][0], "", "");
        // Dune::printmatrix(std::cout, A[1][2], "", "");
        // Dune::printmatrix(std::cout, A[2][1], "", "");
        // Dune::printmatrix(std::cout, A[2][2], "", "");


        // std::cout << "Converted rhs Vector here" << std::endl;

        // Dune::printvector(std::cout, b[0], "", "");
        // Dune::printvector(std::cout, b[1], "", "");
        // Dune::printvector(std::cout, b[2], "", "");

        // std::cout << "Converted sol Vector here" << std::endl;

        // Dune::printvector(std::cout, xx[0], "", "");
        // Dune::printvector(std::cout, xx[1], "", "");
        // Dune::printvector(std::cout, xx[2], "", "");


        // MatCont A_(A);
        // VecCont xx_(xx);
        // VecCont b_(b);

        // std::cout << "PARTIAL B" << std::endl;
        // auto b_sub_ = partial(b_, Dune::index_constant<0>{}, Dune::index_constant<1>{});

        bool pre10, pre20, pre21, ilu1, ilu2, amg1, amg2, uzawa1;
        uzawa1 = getParam<bool>("Preconditioner.Uzawa1", false);
        amg1 = getParam<bool>("Preconditioner.Amg1", false);
        amg2 = getParam<bool>("Preconditioner.Amg2", false);
        ilu1 = getParam<bool>("Preconditioner.Ilu1", true);
        ilu2 = getParam<bool>("Preconditioner.Ilu2", true);
        pre10 = getParam<bool>("Preconditioner.Pre10", false);
        pre21 = getParam<bool>("Preconditioner.Pre21", false);
        pre20 = getParam<bool>("Preconditioner.Pre20", false);

        using Preccontainer = std::vector<Dune::Preconditioner<Vector, Vector>*>;
        using SeqILUPreconditioner = Dune::SeqILU<Matrix,Vector,Vector>;
        using IdentityPreconditioner = Dune::Identity<Vector,Vector>;
        // Dune::SeqJac<Matrix,Vector,Vector> seqJac2(A_[2][2], 1,1.0), seqJac0(A_[0][0], 1,1.0);

        int ilu_n = getParam<int>("Preconditioner.IluN", 0);
        SeqILUPreconditioner seqILU1(A_[1][1], ilu_n, 1.2, true), seqILU0(A_[0][0],ilu_n, 1.2, true);
        std::cout << "ILU-SETUP begin" << std::endl;
        SeqILUPreconditioner seqILU2(A_[2][2], ilu_n, 1.2, true);
        std::cout << "ILU-SETUP end" << std::endl;
        IdentityPreconditioner identprec00, identprec11, identprec22;
        Dune::Zero<Vector,Vector> zeroprec01, zeroprec02, zeroprec10, zeroprec12, zeroprec20, zeroprec21;
        
        Dune::Timer watch(false);

        Dune::StokesOffDiagUpper<Matrix, Vector, Vector, SeqILUPreconditioner, IdentityPreconditioner> stokesOffDiag01(A_[0][1], seqILU1, identprec00);
        Dune::StokesOffDiagUpper<Matrix, Vector, Vector, SeqILUPreconditioner, SeqILUPreconditioner> stokesOffDiag12(A_[1][2], seqILU2, seqILU1);
        using StokesOffDiagLower10 = Dune::StokesOffDiagLower<Matrix, Vector, Vector, SeqILUPreconditioner, IdentityPreconditioner>;
        StokesOffDiagLower10 stokesOffDiag10(A_[1][0], seqILU1, identprec00);
        Dune::StokesOffDiagLower<Matrix, Vector, Vector, SeqILUPreconditioner, StokesOffDiagLower10> stokesOffDiag20(A_[2][1], seqILU2, stokesOffDiag10);
        Dune::StokesOffDiagLower<Matrix, Vector, Vector, SeqILUPreconditioner, SeqILUPreconditioner> stokesOffDiag21(A_[2][1], seqILU2, seqILU1);


        // AMG preconditioner
        using Smoother = Dune::SeqSSOR<Matrix, Vector, Vector>;
        using LinearOperatorAMG = Dune::MatrixAdapter<Matrix, Vector, Vector>;
        using BlockAMG = Dune::Amg::AMG<LinearOperatorAMG, Vector, Smoother>;
        using SmootherArgs = typename Dune::Amg::SmootherTraits<Smoother>::Arguments;

        double smoother_w = getParam<double>("Amg.SmootherW", 1.5);
        int smoother_n = getParam<int>("Amg.SmootherN", 1);
        int maxlevel = getParam<int>("Amg.Maxlevel", 10);
        int num_presmooth = getParam<int>("Amg.NumPresmooth", 40);
        int num_postsmooth = getParam<int>("Amg.NumPostsmooth", 40);
        int defaultvaluesisotropic= getParam<int>("Amg.Defaultvaluesisotropic", 1);
        int coarsetarg= getParam<int>("Amg.CoarsenTarget", 1000);

        SmootherArgs smootherArgs1;
        smootherArgs1.iterations = smoother_n;
        smootherArgs1.relaxationFactor = smoother_w;

        Dune::Amg::Parameters param1;
        param1.setMaxLevel(maxlevel); // maximum number of grid levels (fine grid + number)
        param1.setDefaultValuesIsotropic(defaultvaluesisotropic);
        param1.setGamma(1); // 1: V-Cycle, 2: W-Cycle
        param1.setCoarsenTarget(coarsetarg);
        param1.setNoPreSmoothSteps(num_presmooth); // number of pre smoothing steps
        param1.setNoPostSmoothSteps(num_postsmooth); // number of post smoothing steps

        using Criterion = Dune::Amg::CoarsenCriterion<Dune::Amg::SymmetricCriterion<Matrix, Dune::Amg::FirstDiagonal>>;
        Criterion criterion1(param1);

        LinearOperatorAMG lopAMG11(A_[1][1]);
        BlockAMG bAMG11(lopAMG11, criterion1, smootherArgs1);

        SmootherArgs smootherArgs2;
        smootherArgs2.iterations = smoother_n;
        smootherArgs2.relaxationFactor = smoother_w;
        Dune::Amg::Parameters param2;
        param2.setMaxLevel(maxlevel); // maximum number of grid levels (fine grid + number)
        param2.setDefaultValuesIsotropic(defaultvaluesisotropic);
        param2.setGamma(1); // 1: V-Cycle, 2: W-Cycle
        param2.setNoPreSmoothSteps(num_presmooth); // number of pre smoothing steps
        param2.setNoPostSmoothSteps(num_postsmooth); // number of post smoothing steps
        param2.setCoarsenTarget(coarsetarg);
        Criterion criterion2(param2);
        LinearOperatorAMG lopAMG22(A_[2][2]);
        BlockAMG bAMG22(lopAMG22, criterion2, smootherArgs2);//, smootherArgs11);


        using StokesOffDiagLowerAMG10 = Dune::StokesOffDiagLower<Matrix, Vector, Vector, BlockAMG, IdentityPreconditioner>;
        StokesOffDiagLowerAMG10 stokesOffDiagAMG10(A_[1][0], bAMG11, identprec00);
        Dune::StokesOffDiagLower<Matrix, Vector, Vector, BlockAMG, BlockAMG> stokesOffDiagAMG21(A_[2][1], bAMG22, bAMG11);
        Dune::StokesOffDiagLower<Matrix, Vector, Vector, SeqILUPreconditioner, BlockAMG> stokesOffDiagAMG1Mix21(A_[2][1], seqILU2, bAMG11);
        Dune::StokesOffDiagLower<Matrix, Vector, Vector, BlockAMG, SeqILUPreconditioner> stokesOffDiagAMG2Mix21(A_[2][1], bAMG22, seqILU1);
        Dune::StokesOffDiagLower<Matrix, Vector, Vector, BlockAMG, StokesOffDiagLowerAMG10> stokesOffDiagAMG20(A_[2][1], bAMG22, stokesOffDiagAMG10);
        Dune::StokesOffDiagLower<Matrix, Vector, Vector, SeqILUPreconditioner, StokesOffDiagLowerAMG10> stokesOffDiagAMG1Mix20(A_[2][1], seqILU2, stokesOffDiagAMG10);
        Dune::StokesOffDiagLower<Matrix, Vector, Vector, BlockAMG, StokesOffDiagLower10> stokesOffDiagAMG2Mix20(A_[2][1], bAMG22, stokesOffDiag10);


        using SeqUZAWA1 = Dumux::SeqUzawa<MatCont, VecCont, VecCont, StokesFVGridGeometry>;

        int uzawa_Iters = getParam<int>("Uzawa.Iterations", 1);
        double uzawa_W = getParam<double>("Uzawa.Omega", 0.25);

        std::cout << "PRECONDITIONER PARAMS: amg1=" << amg1 << ", amg2=" << amg2 << ", pre10=" << pre10 << ", pre21=" << pre21 << ", pre20=" << pre20 << std::endl;

        if(ilu2)
            watch.start();


        if(!ilu2)
            watch.start();

        Preccontainer pc;

        //////////////// LN 0

        if (uzawa1)
            pc.push_back(&identprec22);
        else
            pc.push_back(&identprec00);

        if (uzawa1)
            pc.push_back(&identprec22);
        else
            pc.push_back(&zeroprec01);

        pc.push_back(&zeroprec02);

        ///////////// LN 1

        if (pre10){
            if (amg1)
                pc.push_back(&stokesOffDiagAMG10);
            else if (ilu1)
                pc.push_back(&stokesOffDiag10);
            else if (uzawa1)
                pc.push_back(&identprec22);
            else
                pc.push_back(&zeroprec10);
        }
        else if (uzawa1)
            pc.push_back(&identprec22);
        else
            pc.push_back(&zeroprec10);

        if (amg1)
            pc.push_back(&bAMG11);
        else if (ilu1)
            pc.push_back(&seqILU1);
        else if (uzawa1)
            pc.push_back(&identprec22);
        else
            pc.push_back(&identprec11);

        pc.push_back(&zeroprec12);

        /////////////////// LN 2

        if (pre20){
            if(ilu1 && ilu2)
                pc.push_back(&stokesOffDiag20);
            else if(ilu1 && amg2)
                pc.push_back(&stokesOffDiagAMG2Mix20);
            else if(amg1 && ilu2)
                pc.push_back(&stokesOffDiagAMG1Mix20);
            else if (amg1 && amg2)
                pc.push_back(&stokesOffDiagAMG20);
            else
                pc.push_back(&zeroprec20);
        }else
            pc.push_back(&zeroprec20);

        if (pre21){
            if (amg1 && amg2)
                pc.push_back(&stokesOffDiagAMG21);
            else if (ilu1 && ilu2)
                pc.push_back(&stokesOffDiag21);
            else if (amg1 && ilu2)
                pc.push_back(&stokesOffDiagAMG1Mix21);
            else if (ilu1 && amg2)
                pc.push_back(&stokesOffDiagAMG2Mix21);
            else
                pc.push_back(&zeroprec21);
        }
        else
            pc.push_back(&zeroprec21);

        if (amg2)
            pc.push_back(&bAMG22);
        else if (ilu2)
            pc.push_back(&seqILU2);
        else
            pc.push_back(&identprec22);


        using Operator = Dune::MatrixAdapter<MatCont,VecCont,VecCont>;
        std::cout << "New Preconditioner" << std::endl;
        Operator linearOperator(A_);
        Dune::InverseOperatorResult result;
        auto residual = b_;

        // Dune::CGSolver<VecCont> cgsolver(linearOperator, bpc, 1e-8,15, 2, true);

        static const int restart = getParam<int>("Preconditioner.GMRes.Restart", 100);
        static const int maxit = getParam<int>("Preconditioner.GMRes.Iterations", 1200);
        static const double reduction = getParam<double>("Preconditioner.GMRes.Reduction", 1e-16);
        static const std::string outfilename = getParam<std::string>("Preconditioner.GMRes.Outfilename", "GMRes-M_residuals.txt");
        std::cout << "GMRES PARAMS: reduction=" << reduction << ", restart=" << restart << ", maxit=" << maxit << std::endl;

        bool use_pdgmres = getParam<bool>("Preconditioner.PDGMRes.UsePDGMRES", false);
        int pdgmres_minit = getParam<int>("Preconditioner.PDGMRes.MInit", 5);
        int pdgmres_mmin = getParam<int>("Preconditioner.PDGMRes.MMin", 3);
        int pdgmres_mstep = getParam<int>("Preconditioner.PDGMRes.MStep", 3);

        Dune::Timer watchinner(false);
        watchinner.start();

        if (!uzawa1){
            Dune::BlockyPreconditioner<MatCont, VecCont, VecCont, Vector, Vector, Preccontainer> bpc(A_, pc);
            std::cout << "New Preconditioner: NO uzawa" << std::endl;
            if (use_pdgmres){
                std::cout << "Using PD-GMRES and NO uzawa" << std::endl;
                Dune::PDGMResSolver<VecCont> gmressolver(linearOperator, bpc, reduction, pdgmres_minit, pdgmres_mmin, pdgmres_mstep, maxit, 2, true, true);
                std::cout << "outfileupdate" << std::endl;
                gmressolver.update_outfile(outfilename);
                std::cout << "apply" << std::endl;
                gmressolver.apply(xx_,residual,result);
            }
            else{
                std::cout << "Using GMRES and NO uzawa" << std::endl;
                Dune::RestartedGMResSolverXT<VecCont> gmressolver(linearOperator, bpc, reduction, restart, maxit, 2, true, true);
                gmressolver.update_outfile(outfilename);
                gmressolver.apply(xx_,residual,result);
            }
        }
        else{
            Dune::BlockyUzawaPreconditioner<MatCont, VecCont, VecCont, Vector, Vector, Preccontainer, SeqUZAWA1> bupc(A_, pc, true, uzawa_Iters, uzawa_W);
            std::cout << "New Preconditioner: UZAWA" << std::endl;
            if (use_pdgmres){
                std::cout << "Using PD-GMRES and UZAWA" << std::endl;
                Dune::PDGMResSolver<VecCont> gmressolver(linearOperator, bupc, reduction, pdgmres_minit, pdgmres_mmin, pdgmres_mstep, maxit, 2, true, true);
                gmressolver.update_outfile(outfilename);
                gmressolver.apply(xx_,residual,result);
            }
            else{
                std::cout << "Using GMRES and UZAWA" << std::endl;
                Dune::RestartedGMResSolverXT<VecCont> gmressolver(linearOperator, bupc, reduction, restart, maxit, 2, true, true);
                gmressolver.update_outfile(outfilename);
                gmressolver.apply(xx_,residual,result);
            }
        }

        double solvertimeinner = watchinner.elapsed();
        double solvertime = watch.elapsed();
        std::cout << "TIME excl setup: " << solvertimeinner << "s" << std::endl;
        std::cout << "TIME incl setup: " << solvertime << "s" << std::endl;

        if (writetime){
            std::ofstream resfile;
            resfile.open(timefilename, std::ios::out );
            resfile << "TIME excl setup: " << solvertimeinner << " s\n";
            resfile << "TIME incl setup: " << solvertime << " s\n";
            resfile.close();
        }



        for (int i = 0; i < sol[stokesCellCenterIdx].size(); ++i)
            sol[stokesCellCenterIdx][i] = -xx_[stokesCellCenterIdx][i];

        for (int i = 0; i < sol[stokesFaceIdx].size(); ++i)
            sol[stokesFaceIdx][i] = -xx_[stokesFaceIdx][i];

        for (int i = 0; i < sol[darcyIdx].size(); ++i)
            sol[darcyIdx][i] = -xx_[darcyIdx][i];

        assembler->updateGridVariables(sol);
        


        ////////////////////////// End Jenny
    }
    // write vtk output

    stokesVtkWriter.write(1.0);
    darcyVtkWriter.write(1.0);

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
