// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup NavierStokesTests
 * \brief Test for the staggered grid Navier-Stokes model (Donea 2003, \cite Donea2003).
 */

#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

// Jenny Begin

#include <dune/istl/preconditioners.hh>
#include <dune/istl/preconditioner.hh>
#include <dumux/linear/matrixconverter.hh>
// Jenny End

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/nonlinear/staggerednewtonconvergencewriter.hh>

#include "problem.hh"

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::DoneaTest;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    using GridManager = Dumux::GridManager<GetPropType<TypeTag, Properties::Grid>>;
    GridManager gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the problem (boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x;
    x[FVGridGeometry::cellCenterIdx()].resize(fvGridGeometry->numCellCenterDofs());
    x[FVGridGeometry::faceIdx()].resize(fvGridGeometry->numFaceDofs());

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    StaggeredVtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); // Add model specific output fields
    vtkWriter.addField(problem->getAnalyticalPressureSolution(), "pressureExact");
    vtkWriter.addField(problem->getAnalyticalVelocitySolution(), "velocityExact");
    vtkWriter.addFaceField(problem->getAnalyticalVelocitySolutionOnFace(), "faceVelocityExact");
    vtkWriter.write(0.0);

    // use the staggered FV assembler
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);



    nonLinearSolver.assembleLinearSystem(x);

    const auto& M = assembler->jacobian();

    // Dune::printmatrix(std::cout, M[Dune::index_constant<0>{}][Dune::index_constant<0>{}], "", "");
    // Dune::printmatrix(std::cout, M[Dune::index_constant<1>{}][Dune::index_constant<1>{}], "", "");
    // Dune::printmatrix(std::cout, M[Dune::index_constant<0>{}][Dune::index_constant<1>{}], "", "");
    // Dune::printmatrix(std::cout, M[Dune::index_constant<1>{}][Dune::index_constant<0>{}], "", "");

    std::cout << M.N() << std::endl;

    // const auto  A = MatrixConverter<decltype(M)>::multiTypeToBCRSMatrix(M);
    const auto  A = MatrixConverter<decltype(M)>::multiTypeToBcrsBcrsMatrix(M);

    // std::cout << "Converted Matrix here" << std::endl;

    // // Dune::printmatrix(std::cout, A[0][0], "", "");
    // // Dune::printmatrix(std::cout, A[0][1], "", "");
    // // Dune::printmatrix(std::cout, A[1][0], "", "");
    // Dune::printmatrix(std::cout, A[1][1], "", "");

    const auto& rhs = assembler->residual();
    const auto  b = VectorConverter<decltype(rhs)>::multiTypeToBlockBlockVector(rhs);

    // std::cout << "Converted Vector here" << std::endl;

    // Dune::printvector(std::cout, b[0], "", "");
    // Dune::printvector(std::cout, b[1], "", "");

    const auto xx = VectorConverter<decltype(x)>::multiTypeToBlockBlockVector(x);

    // std::cout << "Converted Vector x here" << std::endl;

    // Dune::printvector(std::cout, xx[0], "", "");
    // Dune::printvector(std::cout, xx[1], "", "");

    // Making the Preconditioner

    using Vector = Dune::BlockVector<Dune::FieldVector<double, 1>>;
    using Matrix = Dune::BCRSMatrix<Dune::FieldMatrix<double, 1>>;
    using VecCont = Dune::BlockVector<Vector>;
    using MatCont = Dune::BCRSMatrix<Matrix>;
    // using PrecArgs = Dune::PreconditionerArgs;
    // using IdentArgs = Dune::ArgsIdentity;


    // using P1 = Dune::Preconditioner<Vector,Vector>*;
    // using P2 = Dune::Preconditioner<Vector,Vector>*;
    // using P3 = Dune::Preconditioner<Vector,Vector>*;
    // using P4 = Dune::Preconditioner<Vector,Vector>*;
    // // using PRow = std::tuple<P1, PrecArgs>;
    // // using PMat = std::tuple<PRow, PRow>;


    // using PMV0 = Dune::MultiTypeBlockVector<P1, P2>;
    // using PMV1 = Dune::MultiTypeBlockVector<P3, P4>;
    // using PMMMatrix = Dune::MultiTypeBlockMatrix<PMV0, PMV1>;

    // PMMMatrix blaa;
    // typedef std::tuple<Dune::ArgsIdentity, Dune::ArgsZero> t1;
    // typedef std::tuple<Dune::ArgsZero, Dune::ArgsSeqILU> t2;
    // typedef std::tuple<t1, t2> argtup;
    // argtup at{{Dune::ArgsIdentity(), Dune::ArgsZero()},{Dune::ArgsZero(), Dune::ArgsSeqILU(3, 1.2, true)}};





    // blaa[Dune::index_constant<0>()][Dune::index_constant<0>()] = Dune::ArgsIdentity();
    // blaa[Dune::index_constant<0>()][Dune::index_constant<1>()] = Dune::ArgsZero();
    // blaa[Dune::index_constant<1>()][Dune::index_constant<0>()] = Dune::ArgsZero();
    // blaa[Dune::index_constant<1>()][Dune::index_constant<1>()] = Dune::ArgsSeqILU(3, 1.2, true);


    // IdentArgs identargs(2.0);

    // std::cout << "/////////////////////////" << std::endl;
    // std::cout << identargs.getType() << std::endl;
    // std::cout << identargs.get_w() << std::endl;

    MatCont A_(A);
    VecCont xx_(xx);
    VecCont b_(b);

    using Preccontainer = std::vector<Dune::Preconditioner<Vector, Vector>*>;
    Dune::SeqJac<Matrix,Vector,Vector> seqJac(A_[1][1], 1,1.0);
    using SeqILUPreconditioner = Dune::SeqILU<Matrix,Vector,Vector>;
    SeqILUPreconditioner seqILU(A_[1][1], 3, 1.2, true);
    using IdentityPreconditioner = Dune::Identity<Vector,Vector>;
    IdentityPreconditioner identprec1, identprec2, identprec3;
    Dune::Zero<Vector,Vector> zeroprec1, zeroprec2, zeroprec3;
    Dune::StokesOffDiagUpper<Matrix, Vector, Vector, SeqILUPreconditioner, IdentityPreconditioner> stokesOffDiag01(A_[0][1], seqILU, identprec1);
    Dune::StokesOffDiagLower<Matrix, Vector, Vector, SeqILUPreconditioner, IdentityPreconditioner> stokesOffDiag10(A_[1][0], seqILU, identprec1);

    // PMMMatrix blaa;
    // blaa[Dune::index_constant<0>()][Dune::index_constant<0>()] = &identprec1;
    // blaa[Dune::index_constant<0>()][Dune::index_constant<1>()] = &zeroprec1;
    // blaa[Dune::index_constant<1>()][Dune::index_constant<0>()] = &zeroprec2;
    // blaa[Dune::index_constant<1>()][Dune::index_constant<1>()] = &identprec2;

    Preccontainer pc;
    pc.push_back(&identprec1);
    pc.push_back(&zeroprec1);
    pc.push_back(&stokesOffDiag10);
    pc.push_back(&seqILU);


    Dune::BlockyPreconditioner<MatCont, VecCont, VecCont, Vector, Vector, Preccontainer> bpc(A_, pc);
    // bpc.pre(xx_, b_);
    // bpc.apply(xx_, b_);
    // bpc.post(xx_);

    using Operator = Dune::MatrixAdapter<MatCont,VecCont,VecCont>;
    std::cout << "New Preconditioner" << std::endl;
    Operator linearOperator(A_);
    Dune::InverseOperatorResult result;

    Dune::CGSolver<VecCont> cgsolver(linearOperator, bpc, 1e-8,15, 2, true);
    Dune::RestartedGMResSolver<VecCont> gmressolver(linearOperator, bpc, 1e-16,20, 100, 2);
    Dune::LoopSolver<VecCont> solver(linearOperator, bpc, 1e-8,15,2);
    auto residual = b_;
    std::cout << "New Preconditioner" << std::endl;
    gmressolver.apply(xx_,residual,result);

    // auto __b = VectorConverter<decltype(b)>::multiTypeToBlockVector(b);
    // Dune::printvector(std::cout, __b, "", "");







    //using NewtonConvergenceWriter = StaggeredNewtonConvergenceWriter<FVGridGeometry, SolutionVector>;
    //auto convergenceWriter = std::make_shared<NewtonConvergenceWriter>(*fvGridGeometry);
    //nonLinearSolver.attachConvergenceWriter(convergenceWriter);

    //// linearize & solve
    //Dune::Timer timer;
    //nonLinearSolver.solve(x);

    for (int i = 0; i < x[FVGridGeometry::cellCenterIdx()].size(); ++i)
        x[FVGridGeometry::cellCenterIdx()][i] = -xx_[0][i];

    for (int i = 0; i < x[FVGridGeometry::faceIdx()].size(); ++i)
        x[FVGridGeometry::faceIdx()][i] = -xx_[1][i];

    //// write vtk output
    problem->printL2Error(x);
    vtkWriter.write(1.0);

    //timer.stop();

    //const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    //std::cout << "Simulation took " << timer.elapsed() << " seconds on "
    //          << comm.size() << " processes.\n"
    //          << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
