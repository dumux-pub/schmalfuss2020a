#include <config.h>

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>

// Jenny Begin

// #include <dune/istl/preconditioners.hh>
// #include <dune/istl/preconditioner.hh>
// Jenny End

// #include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/staggeredvtkoutputmodule.hh>
// #include <dumux/linear/seqsolverbackend.hh>
// #include <dumux/nonlinear/newtonsolver.hh>
#include <dumux/nonlinear/staggerednewtonconvergencewriter.hh>

#include "problem.hh"



  /*
  Sample Problem:
  Recursively constructs a tuple of size M, which corresponds to a row of Preconditioners on the input matrix
  */
 //  template<class M, int i=std::tuple_size<M>::value-1>
 //  class PreconditionerTuple{
 //  public:
 //  	// typedef decltype(PreconditionerTuple<M, i - 1>::type) prevTup;
 //  	// typedef decltype(std::tuple_element< i, M>::type) lastelem;
 //  	typedef decltype(std::tuple_cat(std::declval<typename PreconditionerTuple<M, i - 1>::type>(), std::declval<std::tuple<typename std::tuple_element< i, M>::type>>())) type;
 //  };

 //  template<class M>
 //  class PreconditionerTuple<M, 0>{
 //  public:
 //  	typedef std::tuple<typename std::tuple_element<0, M>::type> type;

 //  };

 //  template<size_t I, typename ...Args>
	// constexpr auto get(Dune::MultiTypeBlockVector <Args...>& vec) { return vec[I]; }

	// template<size_t I, typename FirstRow, typename ...Args>
	// constexpr auto get(Dune::MultiTypeBlockMatrix <FirstRow, Args...>& vec) { return vec[I]; }



  /*
	Make MultiTypeBlockVector usable with std::tuple_size for better integration into PreconditionerTupleMatrix / PreconditionerTupleRow
  */
	namespace std {
	  template<typename... Args>
	  class tuple_size<Dune::MultiTypeBlockVector <Args...> > : public std::integral_constant<size_t, sizeof...(Args)> {};

	  template<size_t I, typename... Args>
		class tuple_element< I, Dune::MultiTypeBlockVector <Args...>  > { public: using type = typename std::tuple_element<I,std::tuple<Args...>>::type; };


	  template<typename FirstRow, typename... Args>
	  class tuple_size<Dune::MultiTypeBlockMatrix <FirstRow, Args...> > : public std::integral_constant<size_t, sizeof...(Args) + 1> {};

	  template<size_t I, typename FirstRow, typename... Args>
		class tuple_element< I, Dune::MultiTypeBlockMatrix <FirstRow, Args...>  > { public: using type = typename std::tuple_element<I,std::tuple<FirstRow, Args...>>::type; };
	}




	template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	class TupleAssignment{
	public:
	  static void assign(T& t, Z& z){
	  	std::get<i>(t) = std::get<i>(z);
	  	TupleAssignment<T, Z, i-1>::assign(t, z);
	  }
	};

  template<class T, class Z>
	class TupleAssignment<T, Z, 0u>{
	public:
	  static void assign(T& t, Z& z){
	  	std::get<0>(t) = std::get<0>(z);
	  }
	};


	template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	class TupleMatrixAssignment{
	public:
	  static void assign(T& t, Z& z){
	  	TupleAssignment<typename std::tuple_element< i, T>::type,typename std::tuple_element< i, Z>::type>::assign(std::get<i>(t), std::get<i>(z));
	  	TupleMatrixAssignment<T, Z, i-1>::assign(t, z);
	  }
	};

  template<class T, class Z>
	class TupleMatrixAssignment<T, Z, 0u>{
	public:
	  static void assign(T& t, Z& z){
	  	TupleAssignment<typename std::tuple_element< 0, T>::type,typename std::tuple_element< 0, Z>::type>::assign(std::get<0>(t), std::get<0>(z));
	  }
	};


	
	template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	void assignTuple(T& t, Z& z){
  	TupleAssignment<T, Z, i>::assign(t, z);
  }

  template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	void assignTupleMatrix(T& t, Z& z){
  	TupleMatrixAssignment<T, Z, i>::assign(t, z);
  }




 //  template<class T, class Z, unsigned int i=std::tuple_size<T>::value-1>
	// class TupleGet{
	// public:
	//   static void get(T& t, Z& z, int){
	//   	std::get<i>(t) = std::get<i>(z);
	//   	TupleAssignment<T, Z, i-1>::assign(t, z);
	//   }
	// };








  template<class T, int i=std::tuple_size<T>::value-1>
  class PreconditionerTupleRow{
  public:
  	typedef int currElem;
  	typedef typename PreconditionerTupleRow<T, i - 1>::type prevElems;
  	typedef decltype(std::tuple_cat(std::declval<prevElems>(), std::declval<std::tuple<currElem>>())) type;

  	static const int size = i;

  	// void getElement(int iindex){
  	// 	if (i == iindex){
  	// 		typedef std::get<i>(std::declval<typename this::type>())::type type;
  	// 	}
  	// 	else{
  	// 		typedef std::declval<typename this::type>().get(iindex)::type type;
  	// 	}
  	// }
  };

  template<class T>
  class PreconditionerTupleRow<T, 0>{
  public:
  	typedef int currElem; 
  	typedef std::tuple<currElem> type;

  	static const int size = 0;
  };


  template<class MT, int j=std::tuple_size<MT>::value-1>
  class PreconditionerTupleMatrix{
  public:
  	// typedef decltype(std::tuple_cat(std::declval<typename PreconditionerTupleMatrix<MT, i - 1>::type>(), std::declval<std::tuple<typename PreconditionerTupleRow<std::get<i>(MT)>::type>>())) type;
  	typedef typename PreconditionerTupleRow<typename std::tuple_element< j, MT>::type>::type currElem;
  	typedef decltype(std::tuple_cat(std::declval<typename PreconditionerTupleMatrix<MT, j - 1>::type>(), std::declval<std::tuple<currElem>>())) type;
  };

  template<class MT>
  class PreconditionerTupleMatrix<MT, 0>{
  public:
  	// typedef std::tuple<typename PreconditionerTupleRow<std::get<0>(MT)>::type> type;
  	typedef typename PreconditionerTupleRow<typename std::tuple_element< 0, MT>::type>::type currElem;
  	typedef std::tuple<currElem> type;
  };

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

	// namespace Dune {
	  template<class M, class X, class Y, class PA>
	  class PreconditionerMatrix{
	  public:
	  	typedef typename PreconditionerTupleMatrix<M>::type type;

	  	PreconditionerMatrix (const M& mat, X& x, Y& d, PA& precArgsMat)
      : mat_(mat), x_(x), d_(d), precArgsMat_(precArgsMat) {
      	assignTupleMatrix(precMat_, precArgsMat_);
      }

      static constexpr std::size_t N()
		  {
		    return std::tuple_size<PA>::value;
		  }

		  /** \brief Return the number of matrix rows */
		  static constexpr std::size_t size()
		  {
		    return std::tuple_size<PA>::value;
		  }

		  auto getPreconditionerMatrix(){
		  	return this->precMat_;
		  }

		  // /** \brief Return the number of matrix columns */
		  // static constexpr std::size_t M()
		  // {
		  //   return std::tuple_size<typename std::tuple_element< 0, M>::type>::value;
		  // }

		  template< std::size_t row, std::size_t column >
	    auto get ( const std::integral_constant< std::size_t, row > rowVariable, const std::integral_constant< std::size_t, column > columnVariable )
	    {
	      DUNE_UNUSED_PARAMETER(rowVariable);
	      DUNE_UNUSED_PARAMETER(columnVariable);
	      return std::get<column>(std::get<row>(precMat_));
	    }

	  private:
	  	const M& mat_;
	  	X& x_;
	  	Y& d_;
	  	PA& precArgsMat_;
	  	// this::type precMat_;
	  	typename PreconditionerTupleMatrix<M>::type precMat_;

	  };

	// } 
	// template<size_t I, class M, class X, class Y, class PA>
	// constexpr auto get(PreconditionerMatrix <M, X, Y, PA>& vec) { return std::get<I>(vec.getPreconditionerMatrix()); }

/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////



int main(int argc, char** argv) try
{


	typedef std::tuple<int, double> tup;
	tup t{1, 2.0};
	tup t2{4, 2.0};


  typedef std::tuple<tup, tup> tupmat;
  tupmat tm{t, t2};




  using P1 = int;
  using P2 = int;
  using P3 = int;
  using P4 = int;
  // using PRow = std::tuple<P1, PrecArgs>;
  // using PMat = std::tuple<PRow, PRow>;


  using PMV0 = Dune::MultiTypeBlockVector<P1, P2>;
  PMV0 mbv, mbv2; 
  mbv[std::integral_constant<std::size_t,0>()] = 1111;
  mbv[std::integral_constant<std::size_t,1>()] = 2222;
  using PMV1 = Dune::MultiTypeBlockVector<P3, P4>;
  using PMMMatrix = Dune::MultiTypeBlockMatrix<PMV0, PMV1>;

  PMMMatrix blaa;
  blaa[Dune::index_constant<0>()][Dune::index_constant<0>()] = 111;
  blaa[Dune::index_constant<0>()][Dune::index_constant<1>()] = 222;
  blaa[Dune::index_constant<1>()][Dune::index_constant<0>()] = 333;
  blaa[Dune::index_constant<1>()][Dune::index_constant<1>()] = 444;
  // std::cout << std::get<0>(bv) << std::endl;
  std::cout << std::get<0>(std::get<1>(blaa)) << std::endl;
  std::cout << std::tuple_size<PMMMatrix>::value-1 << std::endl;
  // std::cout << typeid(std::tuple_element<0, PMV0>).name() << std::endl;
  std::cout << std::get<0>(mbv) << std::endl;

  std::tuple_element<1, PMV0>::type primary;
  primary = std::get<1>(mbv);
  std::cout << primary << std::endl;

  std::tuple_element<1, std::tuple_element<1, PMMMatrix>::type>::type lastmat;
  lastmat = std::get<1>(std::get<1>(blaa));
  std::cout << lastmat << std::endl;

  std::tuple_element<1, PMV0>::type primary2;
  primary2 = std::get<1>(mbv2);
  std::cout << primary2 << std::endl;





  PreconditionerTupleRow<PMV0>::type ptr;

  PreconditionerTupleMatrix<PMMMatrix>::type ptm;
  // ptm::currElem::currElem = 24;
  // // std::cout << std::get<0>(ptr) << std::endl;

  std::get<0>(ptr) = 212121;
  std::get<1>(ptr) = 4343434;
  // int i = ptr;
  std::cout << std::get<0>(ptr) << std::endl;

  using Fill = std::tuple<int, int>;
  Fill f{66666,7777}, g{8888,9999};
  using MatFill = std::tuple<Fill, Fill>;
  MatFill h{f,g};

  std::cout << std::tuple_size<PreconditionerTupleRow<PMV0>::type>::value-1 << std::endl;


  // using PM = PreconditionerMatrix<PMMMatrix, PreconditionerTupleRow<PMV0>::type, Fill>;
  // PM pm(blaa, ptr, f);
  assignTuple(ptr, f);
  std::cout << std::get<0>(ptr) << std::endl;
  std::cout << std::get<1>(ptr) << std::endl;
  std::cout << "///////////////////////////////////////" << std::endl;
  assignTupleMatrix(ptm, h);
  std::cout << std::get<0>(std::get<0>(ptm)) << std::endl;
  std::cout << std::get<1>(std::get<0>(ptm)) << std::endl;
  std::cout << std::get<0>(std::get<1>(ptm)) << std::endl;
  std::cout << std::get<1>(std::get<1>(ptm)) << std::endl;
  // std::get<0>(ptr) = std::get<0>(f);


  using PrecMatClass = PreconditionerMatrix<PMMMatrix, tup, tup, MatFill>;
  PrecMatClass pm(blaa, t, t2, h);

  std::cout << std::get<0>(std::get<0>(pm.getPreconditionerMatrix())) << std::endl;
  std::cout << pm.get(std::integral_constant<std::size_t,0>(), std::integral_constant<std::size_t,0>()) << std::endl;
  std::cout << pm.get(std::integral_constant<std::size_t,0>(), std::integral_constant<std::size_t,1>()) << std::endl;
  std::cout << pm.get(std::integral_constant<std::size_t,1>(), std::integral_constant<std::size_t,0>()) << std::endl;
  std::cout << pm.get(std::integral_constant<std::size_t,1>(), std::integral_constant<std::size_t,1>()) << std::endl;



  // ptm 

  // //   spalte       zeile
  // std::get<0>(std::get<1>(ptm)) = ILU;

  



} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
